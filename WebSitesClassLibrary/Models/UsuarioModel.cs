﻿// Clase para la capa de entidades de la tabla Seg_Usuarios
// Fecha de creación martes, 3 de marzo de 2015 19:22:14

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
		  
namespace WebSitesClassLibrary.Models
{
	public class UsuarioModel
	{
		#region "Constantes"
		public struct SPParamNames
		{
			public string Usuario_ID {get{return("@Usuario_ID");}}
			public string Nombre_Usuario {get{return("@Nombre_Usuario");}}
			public string Contraseña {get{return("@Contraseña");}}
			public string Apellido_Paterno {get{return("@Apellido_Paterno");}}
			public string Apellido_Materno {get{return("@Apellido_Materno");}}
			public string Nombre {get{return("@Nombre");}}
			public string Email {get{return("@Email");}}
			public string Multisesion {get{return("@Multisesion");}}
			public string Activo {get{return("@Activo");}}
			public string Orden {get{return("@Orden");}}
			public string Fecha_Creacion {get{return("@Fecha_Creacion");}}
			public string Fecha_Actualizacion {get{return("@Fecha_Actualizacion");}}
			public string Visible {get{return("@Visible");}}
			public string De_Sistema {get{return("@De_Sistema");}}
		}
		public struct SPNames
		{
			public string Insert { get { return ("SP_InsertaSegUsuarios"); } }
			public string Update { get { return ("SP_ActualizaSegUsuarios"); } }
			public string Delete { get { return ("SP_EliminaSegUsuarios"); } }
			public string Get { get { return ("SP_ObtenerSegUsuarios"); } }
		}

		public SPParamNames ParamNames;
		public SPNames StoredProcNames;
		public enum FieldNames { Usuario_ID, Nombre_Usuario, Contraseña, Apellido_Paterno, Apellido_Materno, Nombre, Email, Multisesion, Activo, Orden, Fecha_Creacion, Fecha_Actualizacion, Visible, De_Sistema };

		#endregion

		#region "Propiedades"
		/// <summary>
		/// 
		/// </summary>
		public int Usuario_ID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public string Nombre_Usuario { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public String Contraseña { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public string Apellido_Paterno { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public string Apellido_Materno { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public string Nombre { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public string Email { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public bool Multisesion { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public bool Activo { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public int Orden { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public DateTime Fecha_Creacion { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public DateTime Fecha_Actualizacion { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public bool Visible { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public bool De_Sistema { get; set; }

		#region "Constructor"
		public UsuarioModel()
		{
			Usuario_ID = -1;
			Nombre_Usuario = string.Empty;
			Contraseña = string.Empty;
			Apellido_Paterno = string.Empty;
			Apellido_Materno = string.Empty;
			Nombre = string.Empty;
			Email = string.Empty;
			Multisesion = false;
			Activo = false;
			Orden = -1;
			Fecha_Creacion = new DateTime(2000, 1, 1);
			Fecha_Actualizacion = new DateTime(2000, 1, 1);
			Visible = false;
			De_Sistema = false;
		}
		#endregion
		#endregion 
	}
}


