﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSitesClassLibrary.Models
{
    public class TablaModel
    {
        #region "Constantes"
        public struct SPParamNames
        {
            public string Tabla_ID { get { return ("@Tabla_ID"); } }
            public string Nombre { get { return ("@Nombre"); } }
            public string Descripcion { get { return ("@Descripcion"); } }
            public string Activo { get { return ("@Activo"); } }
            public string Orden { get { return ("@Orden"); } }
            public string Fecha_Creacion { get { return ("@Fecha_Creacion"); } }
            public string Fecha_Actualizacion { get { return ("@Fecha_Actualizacion"); } }
            public string Usuario_ID { get { return ("@Usuario_ID"); } }
            public string Visible { get { return ("@Visible"); } }
        }
        public struct SPNames
        {
            public string Insert { get { return ("SP_InsertaCatTablas"); } }
        }

        public SPParamNames ParamNames;
        public SPNames StoredProcNames;
        public enum FieldNames { Tabla_ID, Nombre, Descripcion, Activo, Orden, Fecha_Creacion, Fecha_Actualizacion, Usuario_ID, Visible };

        #endregion

        #region "Propiedades"
        /// <summary>
        /// 
        /// </summary>
        public int Tabla_ID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Nombre { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Descripcion { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool Activo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Orden { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime Fecha_Creacion { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime Fecha_Actualizacion { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Usuario_ID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool Visible { get; set; }

        #region "Constructor"
        public TablaModel()
        {
            Tabla_ID = -1;
            Nombre = string.Empty;
            Descripcion = string.Empty;
            Activo = false;
            Orden = -1;
            Fecha_Creacion = new DateTime(2000, 1, 1);
            Fecha_Actualizacion = new DateTime(2000, 1, 1);
            Usuario_ID = -1;
            Visible = false;
        }
        #endregion
        #endregion
    }
}