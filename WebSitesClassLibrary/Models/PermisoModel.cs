﻿// Clase para la capa de entidades de la tabla Seg_Permisos
// Fecha de creación sábado, 7 de marzo de 2015 13:29:20

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebSitesClassLibrary.Models
{
	public class PermisoModel
	{
		#region "Constantes"
		public struct SPParamNames
		{
			public string Permiso_ID {get{return("@Permiso_ID");}}
			public string Rol_ID {get{return("@Rol_ID");}}
			public string Acceso_ID {get{return("@Acceso_ID");}}
			public string Nivel_Permiso_ID {get{return("@Nivel_Permiso_ID");}}
			public string Activo {get{return("@Activo");}}
			public string Orden {get{return("@Orden");}}
			public string Fecha_Creacion {get{return("@Fecha_Creacion");}}
			public string Fecha_Actualizacion {get{return("@Fecha_Actualizacion");}}
			public string Usuario_ID {get{return("@Usuario_ID");}}
			public string Visible {get{return("@Visible");}}
			public string De_Sistema {get{return("@De_Sistema");}}
		}
		public struct SPNames
		{
			public string Insert { get { return ("SP_InsertaSegPermisos"); } }
			public string Update { get { return ("SP_ActualizaSegPermisos"); } }
			public string Delete { get { return ("SP_EliminaSegPermisos"); } }
			public string Get { get { return ("SP_ObtenerSegPermisos"); } }
		}

		public SPParamNames ParamNames;
		public SPNames StoredProcNames;
		public enum FieldNames { Permiso_ID, Rol_ID, Acceso_ID, Nivel_Permiso_ID, Activo, Orden, Fecha_Creacion, Fecha_Actualizacion, Usuario_ID, Visible, De_Sistema };

		#endregion

		#region "Propiedades"
		/// <summary>
		/// 
		/// </summary>
		public int Permiso_ID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public int Rol_ID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public int Acceso_ID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public int Nivel_Permiso_ID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public bool Activo { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public int Orden { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public DateTime Fecha_Creacion { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public DateTime Fecha_Actualizacion { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public int Usuario_ID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public bool Visible { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public bool De_Sistema { get; set; }

		#region "Constructor"
		public PermisoModel()
		{
			Permiso_ID = -1;
			Rol_ID = -1;
			Acceso_ID = -1;
			Nivel_Permiso_ID = -1;
			Activo = false;
			Orden = -1;
			Fecha_Creacion = new DateTime(2000, 1, 1);
			Fecha_Actualizacion = new DateTime(2000, 1, 1);
			Usuario_ID = -1;
			Visible = false;
			De_Sistema = false;
		}
		#endregion
		#endregion 
	}
}


