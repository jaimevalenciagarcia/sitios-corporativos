﻿// Clase para la capa de entidades de la tabla Cat_Mensajes
// Fecha de creación martes, 3 de marzo de 2015 18:30:18

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebSitesClassLibrary.Models
{
    public class MensajeModel
    {
        #region "Constantes"
        public struct SPParamNames
        {
            public string Mensaje_ID { get { return ("@Mensaje_ID"); } }
            public string Cve_Mensaje { get { return ("@Cve_Mensaje"); } }
            public string Nombre { get { return ("@Nombre"); } }
            public string Descripcion { get { return ("@Descripcion"); } }
            public string Grupo_Mensaje_ID { get { return ("@Grupo_Mensaje_ID"); } }
            public string Activo { get { return ("@Activo"); } }
            public string Orden { get { return ("@Orden"); } }
            public string Fecha_Creacion { get { return ("@Fecha_Creacion"); } }
            public string Fecha_Actualizacion { get { return ("@Fecha_Actualizacion"); } }
            public string Usuario_ID { get { return ("@Usuario_ID"); } }
            public string Visible { get { return ("@Visible"); } }
        }
        public struct SPNames
        {
            public string Insert { get { return ("SP_InsertaCatMensajes"); } }
        }

        public SPParamNames ParamNames;
        public SPNames StoredProcNames;
        public enum FieldNames { Mensaje_ID, Cve_Mensaje, Nombre, Descripcion, Grupo_Mensaje_ID, Activo, Orden, Fecha_Creacion, Fecha_Actualizacion, Usuario_ID, Visible };

        #endregion

        #region "Propiedades"
        /// <summary>
        /// 
        /// </summary>
        public int Mensaje_ID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Cve_Mensaje { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Nombre { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Descripcion { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Grupo_Mensaje_ID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool Activo { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Orden { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime Fecha_Creacion { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime Fecha_Actualizacion { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Usuario_ID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool Visible { get; set; }

        #region "Constructor"
        public MensajeModel()
        {
            Mensaje_ID = -1;
            Cve_Mensaje = -1;
            Nombre = string.Empty;
            Descripcion = string.Empty;
            Grupo_Mensaje_ID = -1;
            Activo = false;
            Orden = -1;
            Fecha_Creacion = new DateTime(2000, 1, 1);
            Fecha_Actualizacion = new DateTime(2000, 1, 1);
            Usuario_ID = -1;
            Visible = false;
        }
        #endregion
        #endregion
    }
}


