﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebSitesClassLibrary.Models
{
    public class GenericoModel
    {
        #region "Constantes"
        public struct SPParamNames
        {
            public string Nombre_Tabla { get { return ("@Nombre_Tabla"); } }
            public string Nombre_Campo1 { get { return ("@Nombre_Campo1"); } }
            public string Valor_Campo1 { get { return ("@Valor_Campo1"); } }
            public string Visible { get { return ("@Visible"); } }
            public string Activo { get { return ("@Activo"); } }
            public string Operador1 { get { return ("@Operador1"); } }
            public string De_Sistema { get { return ("@De_Sistema"); } }

            public string Nombre_Campo_ID { get { return ("@Nombre_Campo_ID"); } }
            public string Valor_Campo_ID { get { return ("@Valor_Campo_ID"); } }
            public string Nombre_Campo { get { return ("@Nombre_Campo"); } }
            public string Valor_Campo { get { return ("@Valor_Campo"); } }

            public string Nombre_Tabla_Bitacora { get { return ("@Nombre_Tabla_Bitacora"); } }
            public string Tabla_ID { get { return ("@Tabla_ID"); } }
            public string Valor_Anterior { get { return ("@Valor_Anterior"); } }
            public string Valor_Actual { get { return ("@Valor_Actual"); } }
            public string Usuario_ID { get { return ("@Usuario_ID"); } }
            public string Fecha_Modificacion { get { return ("@Fecha_Modificacion"); } }
            public string Orden { get { return ("@Orden"); } }
        }
        public struct SPNames
        {
            public string Select { get { return ("sp_ConsultaGenerico"); } }
            public string Update { get { return ("sp_ActualizaGenerico"); } }

            public string InsertBitacora { get { return ("SP_InsertaEventoBitacora"); } }
        }

        public SPParamNames ParamNames;
        public SPNames StoredProcNames;
        //public enum FieldNames { Mensaje_ID, Cve_Mensaje, Nombre, Descripcion, Grupo_Mensaje_ID, Activo, Orden, Fecha_Creacion, Fecha_Actualizacion, Usuario_ID, Visible };

        #endregion
    }
}