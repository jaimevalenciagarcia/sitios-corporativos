﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Sockets;
using System.Web;
using WebSitesClassLibrary.Models;

namespace WebSitesClassLibrary.DataAccess
{
    public class DASeguridad
    {
        string cadenaConexion = string.Empty;

        public DASeguridad(string cadena)
        {
            cadenaConexion = cadena;
        }

        public int DAIniciarSesion(string usuario, string contraseña)
        {
            SqlConnection con = new SqlConnection(cadenaConexion);
            SqlCommand com = new SqlCommand("sp_IniciarSesion", con);
            com.CommandType = System.Data.CommandType.StoredProcedure;

            SqlParameter userName = new SqlParameter("Nombre_Usuario", usuario);
            SqlParameter userPass = new SqlParameter("Contraseña", contraseña);
            com.Parameters.Add(userName);
            com.Parameters.Add(userPass);

            //primero validamos que la conexion a nivel tcp este correcta

            bool ping = _TryPing(con.DataSource, 1433, 5000);

            if (!ping)
                return -4;

            try
            {
                con.Open();
                int retVal = 0;
                SqlDataReader dr = com.ExecuteReader();

                if (dr != null && dr.Read())
                {
                    retVal = dr.GetInt32(0);
                    dr.Close();
                    //retVal = 0;
                }

                con.Close();
                return retVal;
            }
            catch (SqlException sqlEx)
            {
                //errors = sqlEx.Message;
                Console.WriteLine(sqlEx.Message); 
                return -4;
            }
        }

        public PermisoModel DAValidarPermisos(string Pagina, int Usuario_ID)
        {
            PermisoModel ePermiso = new PermisoModel();
            DataTable dt = new DataTable();

            SqlConnection con = new SqlConnection(cadenaConexion);
            SqlCommand com = new SqlCommand("sp_ValidarPermisos", con);
            com.CommandType = System.Data.CommandType.StoredProcedure;

            SqlParameter spPagina = new SqlParameter("Pagina", Pagina);
            com.Parameters.Add(spPagina);
            SqlParameter spAdendumId = new SqlParameter("Usuario_ID", Usuario_ID);
            com.Parameters.Add(spAdendumId);

            con.Open();

            SqlDataReader dr = com.ExecuteReader();
            dt.Load(dr);

            con.Close();

            ePermiso.Permiso_ID = Convert.ToInt32(dt.Rows[0]["Permiso_Id"].ToString());
            ePermiso.Rol_ID = Convert.ToInt32(dt.Rows[0]["Rol_ID"].ToString());
            ePermiso.Acceso_ID = Convert.ToInt32(dt.Rows[0]["Acceso_Id"].ToString());
            ePermiso.Nivel_Permiso_ID = Convert.ToInt32(dt.Rows[0]["Nivel_Permiso_Id"].ToString());
            ePermiso.Activo = Convert.ToBoolean(dt.Rows[0]["Activo"].ToString());
            ePermiso.Usuario_ID = Convert.ToInt32(dt.Rows[0]["Usuario_ID"].ToString());
            ePermiso.Fecha_Creacion = Convert.ToDateTime(dt.Rows[0]["Fecha_Creacion"].ToString());
            ePermiso.Fecha_Actualizacion = Convert.ToDateTime(dt.Rows[0]["Fecha_Actualizacion"].ToString());

            return ePermiso;
        }

        private static bool _TryPing(string strIpAddress, int intPort, int nTimeoutMsec)
        {
            Socket socket = null;
            try
            {
                socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.DontLinger, false);


                IAsyncResult result = socket.BeginConnect(strIpAddress, intPort, null, null);
                bool success = result.AsyncWaitHandle.WaitOne(nTimeoutMsec, true);

                return socket.Connected;
            }
            catch
            {
                return false;
            }
            finally
            {
                if (null != socket)
                    socket.Close();
            }
        }
    }
}