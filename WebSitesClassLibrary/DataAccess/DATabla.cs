﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data.SqlClient;
using System.Data;
using WebSitesClassLibrary.Models;

namespace WebSitesClassLibrary.DataAccess
{
	public class DATabla
	{
		#region "Properties"
		public string ConnectionString { get; set; }
		public int LastErrorMessageID { get; set; }
		public string LastErrorMessage { get; set; }

		#endregion 

		#region "Constantes"

		private const string NOMBRE_TABLA = "Cat_Tablas";
		private const string NOMBRE_TABLA_BITACORA = "Bitacora_Catalogos";
		private const string NOMBRE_CAMPO_ID = "Tabla_ID";

		#endregion

		#region "Constructor"
	 
		public DATabla(string lsConnectionString)
		{
			LastErrorMessageID = 0;
			LastErrorMessage = string.Empty;
			ConnectionString = lsConnectionString;
		}
		#endregion

		#region "Private Class Functions"

		/// <summary>
		/// Añade los parámetros de un procedimiento almacenado a una lista.
		/// </summary>
		/// <param name="llstParamList">Lista de tipo SQLParameter pasada por referencia</param>
		/// <param name="lsParamName">Nombre del parámetro que ocupa el procedimiento almacenado</param>
		/// <param name="loValue">Valor que va a asignarse al parámentro</param>
		/// <param name="loDataType">Tipo de dato del parámetro en el procedimiento almacenado</param>
		/// <param name="lbItsAnOutputParam">Opcional, True si el parámetro va retornar un valor del procedimiento almacenado, false en caso contrario</param>
		/// <param name="liSize">Opcional, el tamaño del parametro (se usa para parámetros de tipo caracter (char, nchar, varchar, nvarchar, etc)</param>
		private void AddParameterToList(ref List<SqlParameter> llstParamList, string lsParamName, object loValue, SqlDbType loDataType, bool lbItsAnOutputParam = false, int liSize = -1)
		{
			SqlParameter lSQLParam;
			if (liSize != -1)
				lSQLParam = new SqlParameter(lsParamName, loDataType, liSize);
			else
				lSQLParam = new SqlParameter(lsParamName, loDataType);
			lSQLParam.Value = loValue;
			if (lbItsAnOutputParam == true)
				lSQLParam.Direction = ParameterDirection.Output;
			llstParamList.Add(lSQLParam);
		}

		private TablaModel DataTableToEntity(DataTable loTable)
		{
			TablaModel loRegData = new TablaModel();

			if(loTable.Rows.Count > 0)
			{ 
				loRegData.Tabla_ID = Convert.ToInt32(loTable.Rows[0]["Tabla_ID"].ToString());
				loRegData.Nombre = Convert.ToString(loTable.Rows[0]["Nombre"].ToString());
				if (loTable.Rows[0]["Descripcion"] != System.DBNull.Value)
					loRegData.Descripcion = Convert.ToString(loTable.Rows[0]["Descripcion"].ToString());
				loRegData.Activo = Convert.ToBoolean(loTable.Rows[0]["Activo"].ToString());
				loRegData.Orden = Convert.ToInt32(loTable.Rows[0]["Orden"].ToString());
				loRegData.Fecha_Creacion = Convert.ToDateTime(loTable.Rows[0]["Fecha_Creacion"].ToString());
				loRegData.Fecha_Actualizacion = Convert.ToDateTime(loTable.Rows[0]["Fecha_Actualizacion"].ToString());
				loRegData.Usuario_ID = Convert.ToInt32(loTable.Rows[0]["Usuario_ID"].ToString());
				loRegData.Visible = Convert.ToBoolean(loTable.Rows[0]["Visible"].ToString());
			}
			return (loRegData);
		}
		//private DataTable DataReaderToTable(SqlDataReader loSQLReader)
		//{
		//    DataTable loTable = new DataTable();

		//    loTable.Load(loSQLReader);

		//    return (loTable);
		//}
		private List<TablaModel> DataTableToList(DataTable loDataTable)
		{
			List<TablaModel> olstRows = new List<TablaModel>();
			TablaModel loRowData;
			try
			{
				foreach (DataRow dRow in loDataTable.Rows)
				{
					loRowData = new TablaModel();
					loRowData.Tabla_ID = Convert.ToInt32(dRow["Tabla_ID"]);
					loRowData.Nombre = Convert.ToString(dRow["Nombre"]);
					if (dRow["Descripcion"] != System.DBNull.Value)
						loRowData.Descripcion = Convert.ToString(dRow["Descripcion"]);
					loRowData.Activo = Convert.ToBoolean(dRow["Activo"]);
					loRowData.Orden = Convert.ToInt32(dRow["Orden"]);
					loRowData.Fecha_Creacion = Convert.ToDateTime(dRow["Fecha_Creacion"]);
					loRowData.Fecha_Actualizacion = Convert.ToDateTime(dRow["Fecha_Actualizacion"]);
					loRowData.Usuario_ID = Convert.ToInt32(dRow["Usuario_ID"]);
					loRowData.Visible = Convert.ToBoolean(dRow["Visible"]);
					olstRows.Add(loRowData);
				}

				//while (loSQLReader.Read())
				//{
				//    loRowData = new TablaModel();
				//    loRowData.Tabla_ID = Convert.ToInt32(loSQLReader["Tabla_ID"]);
				//    loRowData.Nombre = Convert.ToString(loSQLReader["Nombre"]);
				//    if (loSQLReader["Descripcion"] != System.DBNull.Value)
				//        loRowData.Descripcion = Convert.ToString(loSQLReader["Descripcion"]);
				//    loRowData.Activo = Convert.ToBoolean(loSQLReader["Activo"]);
				//    loRowData.Orden = Convert.ToInt32(loSQLReader["Orden"]);
				//    loRowData.Fecha_Creacion = Convert.ToDateTime(loSQLReader["Fecha_Creacion"]);
				//    loRowData.Fecha_Actualizacion = Convert.ToDateTime(loSQLReader["Fecha_Actualizacion"]);
				//    loRowData.Usuario_ID = Convert.ToInt32(loSQLReader["Usuario_ID"]);
				//    loRowData.Visible = Convert.ToBoolean(loSQLReader["Visible"]);
				//    olstRows.Add(loRowData);
				//}
			}
			catch (Exception ex)
			{
				LastErrorMessage = ex.Message.ToString();
				throw new Exception(LastErrorMessage);
			}
			return (olstRows);
		}

		#endregion

		#region "Public Class Functions and subs"

		/// <summary>
		/// Inserta un registro en la tabla de Cat_Tablas
		/// </summary>
		/// <param name="oTabla">Objeto que contiene los valores a insertar</param>
		/// <returns>True en el caso de que la operación haya fallado, false en caso contrario (ver el valor de la propiedad LastErrorMessage para más información) </returns>
		public int DAOInsertarTabla(TablaModel oTabla)
		{
			TablaModel mTabla = new TablaModel();

			DATabla daTabla = new DATabla(ConnectionString);
			DAGenerico daGenerico = new DAGenerico(ConnectionString);
			DataTable dtInsertedRegInfo = new DataTable();

			SqlConnection loSQLConnection;
			SqlCommand loSQLCommand;
			List<SqlParameter> llstParameters = new List<SqlParameter>();
			
			int liRegIDInserted = -1;
			int liAffectedRows;
			int liOrden = 1;
			int liUsuarioID = -1;
			DateTime dtFechaInsercion;

			AddParameterToList(ref llstParameters, oTabla.ParamNames.Tabla_ID, oTabla.Tabla_ID, SqlDbType.Int, true);
			AddParameterToList(ref llstParameters, oTabla.ParamNames.Nombre, oTabla.Nombre, SqlDbType.NVarChar);
			if (oTabla.Descripcion != string.Empty)
				AddParameterToList(ref llstParameters, oTabla.ParamNames.Descripcion, oTabla.Descripcion, SqlDbType.NVarChar);
			AddParameterToList(ref llstParameters, oTabla.ParamNames.Activo, oTabla.Activo, SqlDbType.Bit);
			AddParameterToList(ref llstParameters, oTabla.ParamNames.Orden, oTabla.Orden, SqlDbType.Int);
			AddParameterToList(ref llstParameters, oTabla.ParamNames.Fecha_Creacion, oTabla.Fecha_Creacion, SqlDbType.DateTime, true);
			AddParameterToList(ref llstParameters, oTabla.ParamNames.Fecha_Actualizacion, oTabla.Fecha_Actualizacion, SqlDbType.DateTime, true);
			AddParameterToList(ref llstParameters, oTabla.ParamNames.Usuario_ID, oTabla.Usuario_ID, SqlDbType.Int);
			AddParameterToList(ref llstParameters, oTabla.ParamNames.Visible, oTabla.Visible, SqlDbType.Bit);

			loSQLConnection = new SqlConnection(ConnectionString);
			loSQLCommand = new SqlCommand(oTabla.StoredProcNames.Insert, loSQLConnection);
			loSQLCommand.CommandType = CommandType.StoredProcedure;
			loSQLCommand.Parameters.AddRange(llstParameters.ToArray());

			try
			{
				loSQLConnection.Open();
				liAffectedRows = loSQLCommand.ExecuteNonQuery();
				if (liAffectedRows > 0)
				{
					oTabla.Tabla_ID = Convert.ToInt32(loSQLCommand.Parameters[oTabla.ParamNames.Tabla_ID].Value);
					oTabla.Fecha_Actualizacion = Convert.ToDateTime(loSQLCommand.Parameters[oTabla.ParamNames.Fecha_Actualizacion].Value);
					oTabla.Fecha_Creacion = Convert.ToDateTime(loSQLCommand.Parameters[oTabla.ParamNames.Fecha_Creacion].Value);
					liRegIDInserted = oTabla.Tabla_ID = Convert.ToInt32(loSQLCommand.Parameters[oTabla.ParamNames.Tabla_ID].Value);

					/* BITACORA */
					dtInsertedRegInfo = DAConsultTable(liRegIDInserted);

					mTabla = daTabla.DAConsultEntity("Nombre", NOMBRE_TABLA);
					dtFechaInsercion = daGenerico.DAOObtenerFechaSistema();
					liUsuarioID = DAGenerico.IDUsuarioActual();

					if (dtInsertedRegInfo.Rows.Count == 1 && dtInsertedRegInfo.Columns.Count > 0)
					{
						for (int liColumnCount = 0; liColumnCount <= dtInsertedRegInfo.Columns.Count - 1; liColumnCount++)
						{
							daGenerico.DAOBitacoraGeneric(NOMBRE_TABLA_BITACORA, mTabla.Tabla_ID, liRegIDInserted, dtInsertedRegInfo.Columns[liColumnCount].ColumnName, null, dtInsertedRegInfo.Rows[0][liColumnCount], liUsuarioID, dtFechaInsercion, liOrden++);
						}
					}
					/* BITACORA */
				}
				else
				{
					LastErrorMessage = "Error en SP al insertar registro, no se insertó ningún registro";
					throw new Exception(LastErrorMessage);
				}
			}
			catch (Exception ex)
			{
				LastErrorMessage = ex.Message.ToString();
				throw new Exception(LastErrorMessage);
			}
			finally
			{
				loSQLConnection.Close();
			}
			return (liRegIDInserted);
		}

		public IEnumerable<TablaModel> DAConsultIEnumerable(int liTablaID = -1)
		{
			DAGenerico oDAGenerico = new DAGenerico(ConnectionString);
			IEnumerable<TablaModel> IEEnuInfo = new List<TablaModel>();
			//SqlDataReader loSQLReader;
			DataTable loDataTable;

			if (liTablaID != -1)
				loDataTable = oDAGenerico.DAOSelectGeneric(NOMBRE_TABLA, DAGenerico.OperadoresSelect.CampoIgualAValor, NOMBRE_CAMPO_ID, Convert.ToString(liTablaID));
			else
				loDataTable = oDAGenerico.DAOSelectGeneric(NOMBRE_TABLA, DAGenerico.OperadoresSelect.Todos);

			IEEnuInfo = DataTableToList(loDataTable);
			return IEEnuInfo;
		}
		public IEnumerable<TablaModel> DAConsultIEnumerable(string lsNombre_Campo, string lsValor)
		{
			DAGenerico oDAGenerico = new DAGenerico(ConnectionString);
			IEnumerable<TablaModel> IEEnuInfo = new List<TablaModel>();
			//SqlDataReader loSQLReader;
			DataTable loDataTable;

			loDataTable = oDAGenerico.DAOSelectGeneric(NOMBRE_TABLA, DAGenerico.OperadoresSelect.CampoIgualAValor, lsNombre_Campo, lsValor);

			IEEnuInfo = DataTableToList(loDataTable);

			return IEEnuInfo;
		}

		public List<TablaModel> DAConsultList(int liTablaID = -1)
		{
			DAGenerico oDAGenerico = new DAGenerico(ConnectionString);
			List<TablaModel> ListInfo = new List<TablaModel>();
			//SqlDataReader loSQLReader;
			DataTable loDataTable;

			if (liTablaID != -1)
				loDataTable = oDAGenerico.DAOSelectGeneric(NOMBRE_TABLA, DAGenerico.OperadoresSelect.CampoIgualAValor, NOMBRE_CAMPO_ID, Convert.ToString(liTablaID));
			else
				loDataTable = oDAGenerico.DAOSelectGeneric(NOMBRE_TABLA, DAGenerico.OperadoresSelect.Todos);

			ListInfo = DataTableToList(loDataTable);
			return ListInfo;
		}
		public List<TablaModel> DAConsultList(string lsNombre_Campo, string lsValor)
		{
			DAGenerico oDAGenerico = new DAGenerico(ConnectionString);
			List<TablaModel> ListInfo = new List<TablaModel>();
			//SqlDataReader loSQLReader;
			DataTable loDataTable;

			loDataTable = oDAGenerico.DAOSelectGeneric(NOMBRE_TABLA, DAGenerico.OperadoresSelect.CampoIgualAValor, lsNombre_Campo, lsValor);

			ListInfo = DataTableToList(loDataTable);

			return ListInfo;
		}

		public DataTable DAConsultTable(int liTablaID = -1)
		{
			DAGenerico oDAGenerico = new DAGenerico(ConnectionString);
			DataTable loTable = new DataTable();
		   // SqlDataReader loSQLReader;
			

			if (liTablaID != -1)
				loTable = oDAGenerico.DAOSelectGeneric(NOMBRE_TABLA, DAGenerico.OperadoresSelect.CampoIgualAValor, NOMBRE_CAMPO_ID, Convert.ToString(liTablaID));
			else
				loTable = oDAGenerico.DAOSelectGeneric(NOMBRE_TABLA, DAGenerico.OperadoresSelect.Todos);

			//loTable = DataReaderToTable(loSQLReader);
			return loTable;
		}
		public DataTable DAConsultTable(string lsNombre_Campo, string lsValor)
		{
			DAGenerico oDAGenerico = new DAGenerico(ConnectionString);
			DataTable loTable = new DataTable();
			//SqlDataReader loSQLReader;

			loTable = oDAGenerico.DAOSelectGeneric(NOMBRE_TABLA, DAGenerico.OperadoresSelect.CampoIgualAValor, lsNombre_Campo, lsValor);

			//loTable = DataReaderToTable(loSQLReader);

			return loTable;
		}

		public TablaModel DAConsultEntity(int liTablaID = -1)
		{
			DAGenerico oDAGenerico = new DAGenerico(ConnectionString);
			TablaModel mModelInfo = new TablaModel();
			//SqlDataReader loSQLReader;
			DataTable loDataTable;

			if (liTablaID != -1)
				loDataTable = oDAGenerico.DAOSelectGeneric(NOMBRE_TABLA, DAGenerico.OperadoresSelect.CampoIgualAValor, NOMBRE_CAMPO_ID, Convert.ToString(liTablaID));
			else
				loDataTable = oDAGenerico.DAOSelectGeneric(NOMBRE_TABLA, DAGenerico.OperadoresSelect.Todos);

			mModelInfo = DataTableToEntity(loDataTable);

			return mModelInfo;
		}
		public TablaModel DAConsultEntity(string lsNombre_Campo, string lsValor)
		{
			DAGenerico oDAGenerico = new DAGenerico(ConnectionString);
			TablaModel mModelInfo = new TablaModel();
			//SqlDataReader loSQLReader;
			DataTable loDataTable;

			loDataTable = oDAGenerico.DAOSelectGeneric(NOMBRE_TABLA, DAGenerico.OperadoresSelect.CampoIgualAValor, lsNombre_Campo, lsValor);

			mModelInfo = DataTableToEntity(loDataTable);

			return mModelInfo;
		}
		

		#endregion
	}
}