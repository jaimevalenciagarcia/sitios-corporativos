﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

using System.Net.Sockets;
using WebSitesClassLibrary.Models;
using WebSitesClassLibrary.DataAccess;

namespace WebSitesClassLibrary.DataAccess
{
    public class DAGenerico
    {
        #region "Properties"
        public string ConnectionString { get; set; }
        public int LastErrorMessageID { get; set; }
        public string LastErrorMessage { get; set; }
        #endregion

        #region "Constantes"

        public enum OperadoresSelect
        {
            Todos = -1,
            CampoIgualAValor = 100,
            CampoNoIgualAValor = 101,
            CampoLikeValor = 200,
            CampoIniciaConValor = 201,
            CampoTerminaConValor = 202,
            CampoNOTLikeValor = 210,
            CampoNOIniciaConValor = 211,
            CampoNOTerminaConValor = 212,
            CampoINValor = 300,
            CampoNOTINValor = 301
        }

        public enum ErroresID
        {
            NoError = 0,
            ConnectionToDBFailed = -1,
            NotImplemented = -2,
            ErrorOnUpdate = -3,
            NoRecordsUpdated = -4,
            ErrorOnSelect = -5
        }

        #endregion

        #region "Constructor"
        public DAGenerico(string lsConnectionString)
        {
            LastErrorMessageID = 0;
            LastErrorMessage = string.Empty;
            ConnectionString = lsConnectionString;
        }
        #endregion

        #region "Private Class Functions
        /// <summary>
        /// Añade los parámetros de un procedimiento almacenado a una lista.
        /// </summary>
        /// <param name="llstParamList">Lista de tipo SQLParameter pasada por referencia</param>
        /// <param name="lsParamName">Nombre del parámetro que ocupa el procedimiento almacenado</param>
        /// <param name="loValue">Valor que va a asignarse al parámentro</param>
        /// <param name="loDataType">Tipo de dato del parámetro en el procedimiento almacenado</param>
        /// <param name="lbItsAnOutputParam">Opcional, True si el parámetro va retornar un valor del procedimiento almacenado, false en caso contrario</param>
        /// <param name="liSize">Opcional, el tamaño del parametro (se usa para parámetros de tipo caracter (char, nchar, varchar, nvarchar, etc)</param>
        private void AddParameterToList(ref List<SqlParameter> llstParamList, string lsParamName, object loValue, SqlDbType loDataType, bool lbItsAnOutputParam = false, int liSize = -1)
        {
            SqlParameter lSQLParam;
            if (liSize != -1)
                lSQLParam = new SqlParameter(lsParamName, loDataType, liSize);
            else
                lSQLParam = new SqlParameter(lsParamName, loDataType);
            lSQLParam.Value = loValue;
            if (lbItsAnOutputParam == true)
                lSQLParam.Direction = ParameterDirection.Output;
            llstParamList.Add(lSQLParam);
        }

        private object GetValueFromObject(object loObject)
        {
            string lsValue = string.Empty;
            if (loObject.GetType().ToString() == "System.Boolean")
                lsValue = Convert.ToString(loObject).Replace("True", "1").Replace("False", "0");
            else if (loObject.GetType().ToString() == "System.DateTime")
                //lsValue = Convert.ToString(loObject).Replace("a.m.", "").Replace("p.m.", "");
                lsValue = Convert.ToString(String.Format("{0:yyyy-MM-dd HH:mm:ss.fff}", loObject));
            else
                lsValue = Convert.ToString(loObject);
            return (lsValue);
        }

        private void ResetLastErrorMessage()
        {
            LastErrorMessageID = 0;
            LastErrorMessage = string.Empty;
        }

        private void SetLastErrorMessage(string lsErrorMessage, ErroresID leErrorID)
        {
            LastErrorMessageID = Convert.ToInt32(leErrorID);
            LastErrorMessage = lsErrorMessage;
        }

        /// <summary>
        /// Ejecuta un procedimiento almacenado 
        /// </summary>
        /// <param name="loSQLConnection">Objeto SQLConnection previamente ya inicializado</param>
        /// <param name="loSQLCommand">Objeto SQLCommand de tipo StoredProcedure y previamente inicializado</param>
        /// <returns>True cuando se afectó al menos un registro, false en caso contrario ó de error</returns>
        private bool ExecuteStoreProc(SqlConnection loSQLConnection, SqlCommand loSQLCommand)
        {
            int liAffectedRows;
            bool lbOpSucceed = false;
            try
            {
                loSQLConnection.Open();
                liAffectedRows = loSQLCommand.ExecuteNonQuery();
                if (liAffectedRows > 0)
                {
                    lbOpSucceed = true;
                }
                else
                {
                    LastErrorMessage = "Error al ejecutar Stored Procedure, no se actualizó ningún registro";
                    throw new Exception(LastErrorMessage);
                }
            }
            catch (Exception ex)
            {
                LastErrorMessage = ex.Message.ToString();
                throw new Exception(LastErrorMessage);
            }
            finally
            {
                loSQLConnection.Close();
            }
            return (lbOpSucceed);
        }

        #endregion

        #region "Public Class Fuctions"

        public DateTime DAOObtenerFechaSistema()
        {
            DateTime FechaSistema;
            DataTable dt = new DataTable();

            SqlConnection loSQLConnection;
            SqlCommand loSQLCommand;

            ResetLastErrorMessage();

            loSQLConnection = new SqlConnection(ConnectionString);
            loSQLCommand = new SqlCommand("sp_ObtenerFechaSistema", loSQLConnection);
            loSQLCommand.CommandType = CommandType.StoredProcedure;


            try
            {
                loSQLConnection.Open();

                SqlDataReader dr = loSQLCommand.ExecuteReader();
                dt.Load(dr);

                loSQLConnection.Close();
            }
            catch (SqlException sqlEx)
            {
                Console.WriteLine(sqlEx.Message.ToString());
                SetLastErrorMessage(sqlEx.Message.ToString(), ErroresID.ConnectionToDBFailed);
            }

            FechaSistema = Convert.ToDateTime(dt.Rows[0]["FechaSistema"].ToString());

            return (FechaSistema);
        }

        public void DAOBitacoraGeneric(string lsNombre_Bitacora, int liTablaID, int liReg_ID_Valor, string lsNombre_Campo, object loValorAnterior, object loValorNuevo, int liUserID, DateTime dtFechaModificacion, int liOrden)
        {
            GenericoModel mGenerico = new GenericoModel();

            bool lbOpSucceed = false;
            SqlConnection loSQLConnection;
            SqlCommand loSQLCommand;
            List<SqlParameter> llstParameters = new List<SqlParameter>();
            string lsValorAnterior = string.Empty;
            string lsValorNuevo = string.Empty;

            AddParameterToList(ref llstParameters, mGenerico.ParamNames.Nombre_Tabla_Bitacora, lsNombre_Bitacora, SqlDbType.NVarChar);
            AddParameterToList(ref llstParameters, mGenerico.ParamNames.Tabla_ID, liTablaID, SqlDbType.Int);
            AddParameterToList(ref llstParameters, mGenerico.ParamNames.Valor_Campo_ID, liReg_ID_Valor, SqlDbType.Int);
            AddParameterToList(ref llstParameters, mGenerico.ParamNames.Nombre_Campo, lsNombre_Campo, SqlDbType.NVarChar);
            if (loValorAnterior != null)
                AddParameterToList(ref llstParameters, mGenerico.ParamNames.Valor_Anterior, GetValueFromObject(loValorAnterior), SqlDbType.NVarChar);
            AddParameterToList(ref llstParameters, mGenerico.ParamNames.Valor_Actual, GetValueFromObject(loValorNuevo), SqlDbType.NVarChar);
            AddParameterToList(ref llstParameters, mGenerico.ParamNames.Usuario_ID, liUserID, SqlDbType.Int);
            AddParameterToList(ref llstParameters, mGenerico.ParamNames.Fecha_Modificacion, dtFechaModificacion, SqlDbType.DateTime);
            AddParameterToList(ref llstParameters, mGenerico.ParamNames.Orden, liOrden, SqlDbType.Int);

            //@Tabla_ID int
            loSQLConnection = new SqlConnection(ConnectionString);
            loSQLCommand = new SqlCommand(mGenerico.StoredProcNames.InsertBitacora, loSQLConnection);
            loSQLCommand.CommandType = CommandType.StoredProcedure;
            loSQLCommand.Parameters.AddRange(llstParameters.ToArray());

            lbOpSucceed = ExecuteStoreProc(loSQLConnection, loSQLCommand);
            //return (lbOpSucceed);
        }

        public DataTable DAOSelectGeneric(string lsNombretabla, OperadoresSelect leOperador = OperadoresSelect.Todos, string lsNombreCampo = "", string lsValorCampo = "", bool lbActivo = true, bool lbVisible = true, bool lbDeSistema = false)
        //public SqlDataReader DAOSelectGeneric(string lsNombretabla, OperadoresSelect leOperador = OperadoresSelect.Todos, string lsNombreCampo = "", string lsValorCampo = "", bool lbActivo = true, bool lbVisible = true, bool lbDeSistema = false)
        {
            MensajeModel mMensaje = new MensajeModel();
            GenericoModel mGenerico = new GenericoModel();

            //SqlDataReader loSQLReader = null;
            DataTable loDataTable = new DataTable();

            SqlConnection loSQLConnection;
            SqlCommand loSQLCommand;
            List<SqlParameter> llstParameters = new List<SqlParameter>();
            //DataTable dtInfo = new DataTable();

            ResetLastErrorMessage();

            AddParameterToList(ref llstParameters, mGenerico.ParamNames.Nombre_Tabla, lsNombretabla, SqlDbType.NVarChar);
            AddParameterToList(ref llstParameters, mGenerico.ParamNames.Nombre_Campo1, lsNombreCampo, SqlDbType.NVarChar);
            AddParameterToList(ref llstParameters, mGenerico.ParamNames.Valor_Campo1, lsValorCampo, SqlDbType.NVarChar);
            AddParameterToList(ref llstParameters, mGenerico.ParamNames.Visible, lbVisible, SqlDbType.Bit);
            AddParameterToList(ref llstParameters, mGenerico.ParamNames.Activo, lbActivo, SqlDbType.Bit);
            AddParameterToList(ref llstParameters, mGenerico.ParamNames.Operador1, Convert.ToInt32(leOperador), SqlDbType.Int);
            AddParameterToList(ref llstParameters, mGenerico.ParamNames.De_Sistema, lbDeSistema, SqlDbType.Bit);

            loSQLConnection = new SqlConnection(ConnectionString);
            loSQLCommand = new SqlCommand(mGenerico.StoredProcNames.Select, loSQLConnection);
            loSQLCommand.CommandType = CommandType.StoredProcedure;
            loSQLCommand.Parameters.AddRange(llstParameters.ToArray());

            try
            {
                loSQLConnection.Open();
                //loSQLReader = loSQLCommand.ExecuteReader();
                loDataTable.Load(loSQLCommand.ExecuteReader());
            }
            catch (Exception ex)
            {
                SetLastErrorMessage("Error en SP al seleccionar registro", ErroresID.ErrorOnSelect);
                Console.WriteLine(ex.Message.ToString());
            }
            finally
            {
                loSQLConnection.Close();
            }
            return (loDataTable);
        }

        public MensajeModel DAOUpdateGeneric(string lsNombretabla, string lsNombreCampoID, string lsValorCampoID, string lsNombreCampo, string lsValorCampo, string lsNombre_Tabla_Bitacora, string lsValorCampoAnt, int liUserID, DateTime dtFechaModificacion, int liOrden)
        {
            int liAffectedRows;

            MensajeModel mMensaje = new MensajeModel();
            GenericoModel mGenerico = new GenericoModel();
            TablaModel mTabla = new TablaModel();

            DATabla daTabla = new DATabla(ConnectionString);

            SqlConnection loSQLConnection;
            SqlCommand loSQLCommand;
            List<SqlParameter> llstParameters = new List<SqlParameter>();

            ResetLastErrorMessage();

            AddParameterToList(ref llstParameters, mGenerico.ParamNames.Nombre_Tabla, lsNombretabla, SqlDbType.NVarChar);
            AddParameterToList(ref llstParameters, mGenerico.ParamNames.Nombre_Campo_ID, lsNombreCampoID, SqlDbType.NVarChar);
            AddParameterToList(ref llstParameters, mGenerico.ParamNames.Valor_Campo_ID, lsValorCampoID, SqlDbType.NVarChar);
            AddParameterToList(ref llstParameters, mGenerico.ParamNames.Nombre_Campo, lsNombreCampo, SqlDbType.NVarChar);
            AddParameterToList(ref llstParameters, mGenerico.ParamNames.Valor_Campo, lsValorCampo, SqlDbType.NVarChar);


            loSQLConnection = new SqlConnection(ConnectionString);
            loSQLCommand = new SqlCommand(mGenerico.StoredProcNames.Update, loSQLConnection);
            loSQLCommand.CommandType = CommandType.StoredProcedure;
            loSQLCommand.Parameters.AddRange(llstParameters.ToArray());

            try
            {
                loSQLConnection.Open();
                liAffectedRows = loSQLCommand.ExecuteNonQuery();
                if (liAffectedRows > 0)
                {
                    mMensaje.Cve_Mensaje = 0;
                    mMensaje.Descripcion = "Usuario Actualizado Correctamente";
                    SetLastErrorMessage(mMensaje.Nombre, ErroresID.NoError);

                    /* BITACORA */
                    mTabla = daTabla.DAConsultEntity("Nombre", lsNombretabla);

                    DAOBitacoraGeneric(lsNombre_Tabla_Bitacora, mTabla.Tabla_ID, Convert.ToInt32(lsValorCampoID), lsNombreCampo, lsValorCampoAnt, lsValorCampo, liUserID, dtFechaModificacion, liOrden);
                    /* BITACORA */
                }
                else
                {
                    mMensaje.Cve_Mensaje = -3;
                    mMensaje.Nombre = "Error Logico en SP al actualizar registro";
                    SetLastErrorMessage(mMensaje.Nombre, ErroresID.NoRecordsUpdated);
                }

            }
            catch (Exception ex)
            {
                mMensaje.Cve_Mensaje = -3;
                mMensaje.Nombre = "Error en SP al actualizar registro";
                Console.WriteLine(ex.Message.ToString());
                SetLastErrorMessage(mMensaje.Nombre, ErroresID.ErrorOnUpdate);
            }
            finally
            {
                loSQLConnection.Close();
            }

            return (mMensaje);
        }

        public MensajeModel DAODeleteGeneric(string lsNombretabla, string lsNombreCampoID, string lsValorCampoID, string lsNombre_Tabla_Bitacora, int liUserID, DateTime dtFechaModificacion)
        {
            MensajeModel mMensaje = new MensajeModel();

            mMensaje = DAOUpdateGeneric(lsNombretabla, lsNombreCampoID, lsValorCampoID, "Visible", "0", lsNombre_Tabla_Bitacora, "1", liUserID, dtFechaModificacion, 1);

            return (mMensaje);
        }

        #endregion

        #region "Public Static Class Fuctions"

        /// <summary>
        /// Compara si el valor de ambos objetos es igual 
        /// </summary>
        /// <param name="oObject1">Primer objeto a comparar</param>
        /// <param name="oObject2">Segundo objeto a comparar</param>
        /// <returns>True si ambos valores son iguales, false en caso contrario</returns>
        public static bool PropertiesAreEquals(object oObject1, object oObject2)
        {
            bool lbPropertyAreEquals = false;
            //Se da por hecho que ambos objetos son del mismo tipo
            if (oObject1 == null || oObject2 == null)
            {
                if (oObject1 == null && oObject2 == null)
                    lbPropertyAreEquals = true;
                else
                    lbPropertyAreEquals = false;
            }
            else
            {
                switch (oObject1.GetType().ToString())
                {
                    case "System.Int32":
                        if (Convert.ToInt32(oObject1) == Convert.ToInt32(oObject2)) lbPropertyAreEquals = true;
                        break;
                    case "System.DateTime":
                    case "System.String":
                        if (Convert.ToString(oObject1) == Convert.ToString(oObject2)) lbPropertyAreEquals = true;
                        break;
                    case "System.Boolean":
                        if (Convert.ToBoolean(oObject1) == Convert.ToBoolean(oObject2)) lbPropertyAreEquals = true;
                        break;
                    case "System.Decimal":
                        if (Convert.ToDecimal(oObject1) == Convert.ToDecimal(oObject2)) lbPropertyAreEquals = true;
                        break;
                    case "System.Double":
                        if (Convert.ToDouble(oObject1) == Convert.ToDouble(oObject2)) lbPropertyAreEquals = true;
                        break;
                    case "System.Int16":
                        if (Convert.ToInt16(oObject1) == Convert.ToInt16(oObject2)) lbPropertyAreEquals = true;
                        break;
                    case "System.Byte[]":
                        if (Convert.ToByte(oObject1) == Convert.ToByte(oObject2)) lbPropertyAreEquals = true;
                        break;
                    case "System.Int64":
                        if (Convert.ToInt64(oObject1) == Convert.ToInt64(oObject2)) lbPropertyAreEquals = true;
                        break;
                    default:
                        lbPropertyAreEquals = false;
                        break;
                }
            }
            return (lbPropertyAreEquals);
        }

        public static bool AccesoPagina(string lscadena, string lsPagina)
        {
            bool resultado;
            resultado = BLValidaAcceso(lscadena, lsPagina);

            return resultado;
        }

        public static bool BLValidaAcceso(string cadena, string Pagina)
        {
            DASeguridad daSeguridad = new DASeguridad(cadena);
            UsuarioModel eUsuario = new UsuarioModel();
            PermisoModel ePermiso = new PermisoModel();

            eUsuario = (UsuarioModel)System.Web.HttpContext.Current.Session["Usuario"];

            ePermiso = daSeguridad.DAValidarPermisos(Pagina, eUsuario.Usuario_ID);

            return ePermiso.Activo;
        }

        public static bool _TryPing(string strIpAddress, int intPort, int nTimeoutMsec)
        {
            Socket socket = null;
            try
            {
                socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.DontLinger, false);


                IAsyncResult result = socket.BeginConnect(strIpAddress, intPort, null, null);
                bool success = result.AsyncWaitHandle.WaitOne(nTimeoutMsec, true);

                return socket.Connected;
            }
            catch
            {
                return false;
            }
            finally
            {
                if (null != socket)
                    socket.Close();
            }
        }

        public static int IDUsuarioActual()
        {
            UsuarioModel mUsuario = new UsuarioModel();
            if (HttpContext.Current.Session["Usuario"] != null)
                mUsuario = (UsuarioModel)HttpContext.Current.Session["Usuario"];
            
            return (mUsuario.Usuario_ID);
        }

        #endregion
    }
}