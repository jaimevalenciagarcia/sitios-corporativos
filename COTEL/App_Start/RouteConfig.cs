﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace COTEL
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Miracle", action = "Index", id = UrlParameter.Optional }
            );

            //routes.MapRoute(
            //    name: "Terminos",
            //    url: "{controller}",
            //    defaults: new { controller = "Terminos", action = "Terminos"}
            //);
            
            //routes.MapRoute(
            //    name: "Miracle",
            //    url: "contact-us/{action}/{id}",
            //    defaults: new { controller = "Miracle", action = "ConektaTest", id = UrlParameter.Optional }
            //);

            //routes.MapRoute(
            //    "Contact Us",
            //    "contact-us/{action}/",
            //    new { controller = "ContactUs", action = "Default" }
            //);
            //routes.MapRoute(
            //    name: "Default",
            //    url: "{action}/{item}",
            //    defaults: new { controller = "Miracle", action = "ConektaTest", item = UrlParameter.Optional }
            //);

        }
    }
}