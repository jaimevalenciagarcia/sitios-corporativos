﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using COTEL.Models;
using System.ComponentModel.DataAnnotations;
namespace COTEL.ViewModels
{
    public class RecargaTAEViewModel
    {
        public RecargaInfoModel RecargaInfo { get; set; }
        public CreditCardModel TarjetaCreditoInfo { get; set; }
        public RecargaTelcelModel RecargaTelcelInfo { get; set; }
        public string Token { get; set; }
        public string ErrorString { get; set; }
        public string RespuestaFolioCONEKTA { get; set; }
        public bool CobroRealizado { get; set; }

        [Required(ErrorMessage = "No se ha especificado el código CAPTCHA")]
        public string CodigoCaptcha { get; set; }
        public RecargaTAEViewModel()
        {
            RecargaInfo = new RecargaInfoModel();
            TarjetaCreditoInfo = new CreditCardModel();
            RecargaTelcelInfo = new RecargaTelcelModel();
            Token = string.Empty;
            RespuestaFolioCONEKTA = string.Empty;
        }
    }
}