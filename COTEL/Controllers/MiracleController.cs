﻿using COTEL.DataAccess;
using COTEL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using WebSitesClassLibrary.GeneralPurpose;
using System.Diagnostics;

using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Formatting;
using Newtonsoft.Json;
using COTEL.ViewModels;
using COTEL.Classes;

namespace COTEL.Controllers
{
    public class MiracleController : Controller
    {
        private string msConnectionString = WebConfigurationManager.ConnectionStrings["SitiosConn"].ConnectionString;
        //private RecargaInfoModel moRecargaTAE = new RecargaInfoModel();
        //
        // GET: /Miracle/
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult AboutUs()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ProductsAndServices()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Proyectos()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Blog()
        {
            return View();
        }
        
        [HttpGet]
        public ActionResult Contacto()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ConektaTest()
        {
            RecargaTAEViewModel loRecargaModel = new RecargaTAEViewModel();
            loRecargaModel.RecargaInfo.ActualStep = RecargaInfoModel.CurrentStep.StepGetPhoneNumber; //.StepGetPhoneNumber;
            if (COTELConsumer.SANDBOX_MODE)
            {
                loRecargaModel.RecargaInfo.NumeroCelular = "5548409722";
                loRecargaModel.RecargaInfo.ConfirmacionCelular = "5548409722";
                loRecargaModel.TarjetaCreditoInfo.TarjetaHabiente = "";// "Valencia García Jaime Alejandro";
                loRecargaModel.TarjetaCreditoInfo.NumeroTarjeta = ""; // "4000000000000127";
                loRecargaModel.TarjetaCreditoInfo.CVC = ""; //"258";
                loRecargaModel.TarjetaCreditoInfo.ExpiracionAño = ""; // "2020";
                loRecargaModel.TarjetaCreditoInfo.ExpiracionMes = ""; // "10";
                loRecargaModel.TarjetaCreditoInfo.CorreoElectronico = ""; // "jvalencia@e-bitware.com";
            }
            Session["MessageText"] = "";
            //loRecargaModel.ErrorString = "Recarga realizada con éxito";
            //Revisar si las recargas pueden realizarse.
            if (COTELConsumer.SaldoSuficiente(10))
                return View(loRecargaModel);
            else
                return View("RecargasOffline"); 

        }

        [HttpGet]
        public ActionResult generateCaptcha()
        {
            Random rnd = new Random();
            int liFirstWord = rnd.Next(4, 5);
            int liSecondWord = rnd.Next(2, 3);

            System.Drawing.FontFamily family = new System.Drawing.FontFamily("Tahoma");
            CaptchaImage img = new CaptchaImage(150, 50, family);
            //string text = img.CreateRandomText(4) + " " + img.CreateRandomText(3);
            string text = img.CreateRandomText(liFirstWord);// +" " + img.CreateRandomText(liSecondWord);
            img.SetText(text);
            img.GenerateImage();
            img.Image.Save(Server.MapPath("~") + this.Session.SessionID.ToString() + ".png", System.Drawing.Imaging.ImageFormat.Png);
            Session["captchaText"] = text;
            return Json(this.Session.SessionID.ToString() + ".png?t=" + DateTime.Now.Ticks, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ConektaTest(RecargaTAEViewModel loRecargaModel, string Command)
        {
            string lsErrorMessage = string.Empty;
            CustomEmailEngine01 loEmailEngine = new CustomEmailEngine01();
            ValidacionesSistema loValidaciones = new ValidacionesSistema();
            Session["MessageText"] = "";
            switch (loRecargaModel.RecargaInfo.ActualStep)
            { 
                case RecargaInfoModel.CurrentStep.StepGetPhoneNumber:
                    break;
                case RecargaInfoModel.CurrentStep.StepGetCreditCard:
                    //Validar que no se venda más de x cantidad a esa tarjeta
                    //Validar que la tarjeta no se use más de n veces.
                    //Si las pruebas son superadas mandar correo con un NIP para que el usuario pueda concluir su compra
                    
                    if (!loEmailEngine.NIPSentTo(loRecargaModel.TarjetaCreditoInfo))
                    {
                        loRecargaModel.ErrorString = loEmailEngine.LastErrorMessage;
                        Session["MessageText"] = loEmailEngine.LastErrorMessage; 
                    }
                    else
                    {
                        loRecargaModel.ErrorString = null;
                    }
                    break;
                case RecargaInfoModel.CurrentStep.StepResume:
                    if (Command != null) break; //Es null cuando se presionó el botón de "Realizar Recarga"
                    
                    if (Request.Form["clientCaptcha"].ToString() != Session["captchaText"].ToString()) 
                    {
                        loRecargaModel.ErrorString = "Codigo Captcha inválido";
                    }
                    
                    if (Request.Form["clientNIP"].ToString() != loValidaciones.ObtenerNIPActual(loRecargaModel.TarjetaCreditoInfo))
                    {
                        loRecargaModel.ErrorString += ", NIP inválido";
                    }
                    
                    if(loRecargaModel.ErrorString == null)
                    { 
                        if (COTELConsumer.SaldoSuficiente(loRecargaModel.RecargaInfo.MontoRecarga))
                        {
                            //Revisar que se haya insertado correctamente el NIP

                            string lsTokenID = Request.Form["conektaTokenID"].ToString();
                            //loRecargaModel.RecargaInfo.MontoRecarga = loRecargaModel.RecargaInfo.MontoRecarga * 100;//La API de conekta no acepta puntos decimales
                            loRecargaModel.Token = lsTokenID;
                            var loTask = ConsumeTCChargeConektaAPI(loRecargaModel);
                            while (!loTask.IsCompleted)
                            {
                                Debug.Print("Not Completed!!: " + loTask.IsCompleted.ToString());
                            }
                            Debug.Print(""); //Consumir servicio de COTEL para realizar la recarga correspondiente.
                            if(loRecargaModel.CobroRealizado)
                            { 
                                loRecargaModel.RecargaTelcelInfo = COTELConsumer.AplicarRecarga(loRecargaModel.RecargaInfo.NumeroCelular, loRecargaModel.RecargaInfo.MontoRecarga);
                                loValidaciones.RegistrarVenta(loRecargaModel.TarjetaCreditoInfo, loRecargaModel.RecargaInfo.MontoRecarga);
                                
                                
                             
                                
                                
                                loEmailEngine.SendRecargaInfoTo(loRecargaModel.RecargaTelcelInfo, loRecargaModel.TarjetaCreditoInfo, loRecargaModel.RecargaInfo,  loRecargaModel.RespuestaFolioCONEKTA);
                                loRecargaModel.ErrorString = "Proceso terminado con éxito $" + loRecargaModel.RecargaInfo.MontoRecarga + " fueron abonados al número " + loRecargaModel.RecargaInfo.NumeroCelular + " " +
                                                          "Respuesta de TELCEL: " + loRecargaModel.RecargaTelcelInfo.TextoRespuesta + " " +
                                                          "Folio COTEL: " + loRecargaModel.RecargaTelcelInfo.FolioCotel + " " +
                                                          "Folio TELCEL: " + loRecargaModel.RecargaTelcelInfo.FolioTelcel + " " + 
                                                          "Folio CONEKTA: " + loRecargaModel.RespuestaFolioCONEKTA;


                                Command = "ApplyCharge";
                            }
                        }
                        else
                        {
                            loRecargaModel.ErrorString = "Error al realizar recarga, fondos insuficientes en el sistema de TAE, no se efectuó ningún movimiento a la tarjeta, por favor intente más tarde.";
                        }
                    }
                    if (loRecargaModel.ErrorString != null)
                        Session["MessageText"] = loRecargaModel.ErrorString;
                    break;
                default:
                    break;
            }

            if (loRecargaModel.ErrorString == null)
            { 
                switch (Command)
                {
                    case "NextSelectionPhone":
                        loRecargaModel.RecargaInfo.ActualStep = RecargaInfoModel.CurrentStep.StepGetCreditCard;
                        break;
                    case "BackConfirmacion":
                        loRecargaModel.RecargaInfo.ActualStep = RecargaInfoModel.CurrentStep.StepGetCreditCard;
                        break;
                    case "BackCreditCard":
                        loRecargaModel.RecargaInfo.ActualStep = RecargaInfoModel.CurrentStep.StepGetPhoneNumber;
                        break;
                    case "NextCreditCard":
                        loRecargaModel.RecargaInfo.ActualStep = RecargaInfoModel.CurrentStep.StepResume;
                        break;

                    case "ApplyCharge":
                        loRecargaModel.RecargaInfo.ActualStep = RecargaInfoModel.CurrentStep.StepShowResults;
                        break;
                    case "Finish":
                        loRecargaModel = new RecargaTAEViewModel();
                        loRecargaModel.RecargaInfo.ActualStep = RecargaInfoModel.CurrentStep.StepGetPhoneNumber;
                        break;
                }
            }
            else if (Command == "ApplyCharge")
            {
                loRecargaModel.RecargaInfo.ActualStep = RecargaInfoModel.CurrentStep.StepShowResults;
            }
            return View(loRecargaModel);   
        }

        public async System.Threading.Tasks.Task<ActionResult> ConsumeTCChargeConektaAPI(RecargaTAEViewModel loRecargaModel)
        {
            using (var client = new HttpClient())
            {

                ConektaPagoTarjeta loPagoTarjeta = new ConektaPagoTarjeta();
                ConektaDetalles loDetalle = new ConektaDetalles();
                ConektaItem loItem = new ConektaItem();
                
                HttpResponseMessage response;

                //HTTP GET
                try
                {
                    //----------- Poblar el objeto Pago ----------------
                    loPagoTarjeta.description = "TAE";
                    loPagoTarjeta.amount = loRecargaModel.RecargaInfo.MontoRecarga * 100; 
                    loPagoTarjeta.currency = "MXN";
                    loPagoTarjeta.reference_id = "9839-TAE"; //Para control interno de la tienda
                    loPagoTarjeta.card = loRecargaModel.Token;
                    loPagoTarjeta.details = new List<ConektaDetalles>();

                    loItem.name = "Tiempo Aire Electrónico";
                    loItem.sku = "TAE20150502";
                    loItem.unit_price = loRecargaModel.RecargaInfo.MontoRecarga * 100; 
                    loItem.description = "Recarga de TAE para Telcel";
                    loItem.quantity = 1;
                    loItem.type = "Telefonía Celular";

                    loDetalle.name = loRecargaModel.TarjetaCreditoInfo.TarjetaHabiente;
                    loDetalle.email = "";
                    loDetalle.phone = loRecargaModel.RecargaInfo.NumeroCelular;

                    loDetalle.line_items.Add(loItem);
                    loPagoTarjeta.details.Add(loDetalle);


                    String rcString = JsonConvert.SerializeObject(loPagoTarjeta);
                    
                    client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "application/vnd.conekta-v0.3.0+json");
                    client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "Basic a2V5X0NYZFFyR0MyeFB0THp4M1ZxZnljenJR");
                    client.Timeout = new TimeSpan(0, 0, 30);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, "https://api.conekta.io/charges");
                    
                    req.Content = new StringContent(rcString, Encoding.UTF8, "application/json");
                    Debug.Print("------------");
                    
                    response = await client.SendAsync(req).ConfigureAwait(false);
                    if (response.IsSuccessStatusCode)
                    {
                        Debug.Print("Exito!!");
                        var jsonString = await response.Content.ReadAsStringAsync();
                        Debug.Print(jsonString);
                        var loPaymentInfo = JsonConvert.DeserializeObject<ConektaPaymentInfo>(jsonString);
                        //loRecargaModel.ErrorString = "Cobro realizado con éxito. $" + (loRecargaModel.RecargaInfo.MontoRecarga) + " al número " + loRecargaModel.RecargaInfo.NumeroCelular;
                        loRecargaModel.CobroRealizado = true;
                        loRecargaModel.RespuestaFolioCONEKTA = loPaymentInfo.id; //  555a134224122900e000f109
                        //TempData["msg"] = "<script>alert('Change succesfully');</script>";
                        //Page.ClientScript.RegisterStartupScript(this.GetType(), "ErrorAlert", "alert('Some text here - maybe ex.Message');", true);
                        //return Redirect("http://www.google.com"); 
                        return RedirectToAction("ConektaTest", loRecargaModel);
                    }
                    else
                    {
                        Debug.Print("Error en tarjeta");
                        var jsonString = await response.Content.ReadAsStringAsync();
                        Debug.Print(jsonString);
                        var loErrorInfo = JsonConvert.DeserializeObject<ConektaError>(jsonString);
                        loRecargaModel.ErrorString = loErrorInfo.message_to_purchaser;
                        loRecargaModel.CobroRealizado = false;
                        //TempData["msg"] = "<script>alert('Change succesfully');</script>";
                        //Page.ClientScript.RegisterStartupScript(this.GetType(), "ErrorAlert", "alert('Some text here - maybe ex.Message');", true);
                        //return Redirect("http://www.google.com");
                        return RedirectToAction("ConektaTest", loRecargaModel);
                    }
                }
                catch (HttpRequestException Exception)
                {
                    Console.WriteLine("ERROR");
                    Console.WriteLine(Exception.Message);
                    Console.ReadKey();
                }

                //return View("ConektaTest", loRecargaModel);
            }
            return RedirectToAction("ConektaTest", loRecargaModel);
        }

        //public async System.Threading.Tasks.Task<ActionResult> ConsumeExternalAPI()
        //{
        //    string url = "http://crudwithwebapi.azurewebsites.net/api/serverdata/2"; 

        //    using (System.Net.Http.HttpClient client = new System.Net.Http.HttpClient())
        //    {
        //        client.BaseAddress = new Uri(url);
        //        client.DefaultRequestHeaders.Accept.Clear();
        //        client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

        //        System.Net.Http.HttpResponseMessage response = await client.GetAsync(url);
        //        if (response.IsSuccessStatusCode)
        //        {
        //            var data = await response.Content.ReadAsStringAsync();
        //            var table = Newtonsoft.Json.JsonConvert.DeserializeObject<System.Data.DataTable>(data);

        //            System.Web.UI.WebControls.GridView gView = new System.Web.UI.WebControls.GridView();
        //            gView.DataSource = table;
        //            gView.DataBind();
        //            using (System.IO.StringWriter sw = new System.IO.StringWriter())
        //            {
        //                using (System.Web.UI.HtmlTextWriter htw = new System.Web.UI.HtmlTextWriter(sw))
        //                {
        //                    gView.RenderControl(htw);
        //                    ViewBag.ReturnedData = sw.ToString();
        //                }
        //            }
        //        }
        //    }
        //    return View();
        //}

        //[HttpPost]
        //public ActionResult EmailRequest(EmailRequest model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        // save to db, for instance
        //        return RedirectToAction("AnotherAction");
        //    }
        //    return View();
        //}

        [HttpPost]
        public ActionResult Contacto(ContactModel loModel)
        {
            CompañiaModel mCompany = new CompañiaModel();
            ConfiguracionModel mConfiguration = new ConfiguracionModel();

            Mail clsMail = new Mail();

            bool MailEnviado;
            string sBody;

            if (ModelState.IsValid)
            {
                DACompañia daCompany = new DACompañia(msConnectionString);
                //DATema daTheme = new DATema(cadena);
                DAConfiguracion daConfiguration = new DAConfiguracion(msConnectionString);

                mCompany = daCompany.DAOConsultCompany(Constants.Constants.RFC);
                //mTheme = daTheme.DAConsultEntity(mCompany.Tema_ID);
                mConfiguration = daConfiguration.DAConsultEntity(mCompany.Configuracion_ID);

                clsMail.ConfigurarMail(mConfiguration.Correo_De, mConfiguration.Nombre_Mostrar, mConfiguration.Usuario, mConfiguration.Contraseña, mConfiguration.Puerto, mConfiguration.Servidor_SMTP);

                sBody = clsMail.FormatBodyTemplateNice(mConfiguration.Titulo_Correo, loModel.Nombre_Contacto, loModel.Email_Contacto, loModel.Numero_Contacto, loModel.Mensaje_Contacto);  //  contact_name, contact_email, contact_number, contact_message);

                MailEnviado = clsMail.EnviarMail(mConfiguration.Correo_Para, mConfiguration.Asunto, sBody, mConfiguration.Formato_Html);

                return RedirectToAction("Contacto");
            }
            else
            {
                return PartialView("Contacto", loModel);
            }
        }
    }
}
