﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COTEL.Models
{
    public class RecargaTelcelModel
    {
        public string FechaHoraRecarga {get; set;}
        public string TextoRespuesta {get; set;}
        public ulong FolioCotel {get; set;}
        public ulong FolioTelcel {get; set;}
        public decimal MontoAbonado {get; set;}
        public string Telefono {get; set;}

        public RecargaTelcelModel()
        {
            FechaHoraRecarga = "2001-01-01T00:00:00.00-00:00";
            TextoRespuesta = string.Empty;
            FolioCotel = 0;
            FolioTelcel = 0;
            MontoAbonado = 0;
            Telefono = string.Empty;
        }
    }
}