﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace COTEL.Models
{
    public class RecargaInfoModel
    {
        public enum CurrentStep { UnknownStep = -1, StepGetPhoneNumber, StepGetCreditCard, StepResume, StepShowResults}
        public CurrentStep ActualStep { get; set; }


        [Required(ErrorMessage = "No se ha introducido el número celular")]
        [StringLength(10, ErrorMessage = "El número no puede contener más de 10 caracteres.")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Numero celular inválido")]
        [Compare("ConfirmacionCelular", ErrorMessage = "El número y la confirmación no concuerdan")]
        public string NumeroCelular { get; set; }

        [Required(ErrorMessage = "No se ha introducido el número celular")]
        [StringLength(10, ErrorMessage = "El número no puede contener más de 10 caracteres.")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Numero celular inválido")]
        [Compare("NumeroCelular", ErrorMessage = "El número y la confirmación no concuerdan.")]
        public string ConfirmacionCelular { get; set; }


        [Required(ErrorMessage = "No se ha introducido el monto de la recarga.")]
        [Range(20, 500, ErrorMessage = "No se ha introducido el monto de la recarga.")]
        public int MontoRecarga { get; set; }
        
        
        
        public RecargaInfoModel()
        {
            ActualStep = CurrentStep.UnknownStep;
        }
    }
}