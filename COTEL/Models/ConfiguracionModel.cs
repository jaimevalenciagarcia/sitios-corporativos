﻿// Clase para la capa de Modelos de la tabla Cat_Configuraciones
// Fecha de creación viernes, 27 de marzo de 2015 01:24:10 p. m.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COTEL.Models
{
    public class ConfiguracionModel
    {
        #region "Constantes"
        public struct SPParamNames
        {
            public string Configuracion_ID { get { return ("@Configuracion_ID"); } }
            public string Nombre { get { return ("@Nombre"); } }
            public string Servidor_SMTP { get { return ("@Servidor_SMTP"); } }
            public string Puerto { get { return ("@Puerto"); } }
            public string Usuario { get { return ("@Usuario"); } }
            public string Contraseña { get { return ("@Contraseña"); } }
            public string Correo_De { get { return ("@Correo_De"); } }
            public string Nombre_Mostrar { get { return ("@Nombre_Mostrar"); } }
            public string Correo_Para { get { return ("@Correo_Para"); } }
            public string Correo_CC { get { return ("@Correo_CC"); } }
            public string Asunto { get { return ("@Asunto"); } }
            public string Titulo_Correo { get { return ("@Titulo_Correo"); } }
            public string Formato_Html { get { return ("@Formato_Html"); } }
            public string Activo { get { return ("@Activo"); } }
            public string Orden { get { return ("@Orden"); } }
            public string Fecha_Creacion { get { return ("@Fecha_Creacion"); } }
            public string Fecha_Actualizacion { get { return ("@Fecha_Actualizacion"); } }
            public string Usuario_ID { get { return ("@Usuario_ID"); } }
            public string Visible { get { return ("@Visible"); } }
        }
        public struct SPNames
        {
            public string Insert { get { return ("SP_InsertaCatConfiguraciones"); } }
        }

        public SPParamNames ParamNames;
        public SPNames StoredProcNames;
        public enum FieldNames { Configuracion_ID, Nombre, Servidor_SMTP, Puerto, Usuario, Contraseña, Correo_De, Nombre_Mostrar, Correo_Para, Correo_CC, Asunto, Titulo_Correo, Formato_Html, Activo, Orden, Fecha_Creacion, Fecha_Actualizacion, Usuario_ID, Visible };

        #endregion

        #region "Propiedades"
        public int Configuracion_ID { get; set; }
        public string Nombre { get; set; }
        public string Servidor_SMTP { get; set; }
        public int Puerto { get; set; }
        public string Usuario { get; set; }
        public string Contraseña { get; set; }
        public string Correo_De { get; set; }
        public string Nombre_Mostrar { get; set; }
        public string Correo_Para { get; set; }
        public string Correo_CC { get; set; }
        public string Asunto { get; set; }
        public string Titulo_Correo { get; set; }
        public bool Formato_Html { get; set; }
        public bool Activo { get; set; }
        public int Orden { get; set; }
        public DateTime Fecha_Creacion { get; set; }
        public DateTime Fecha_Actualizacion { get; set; }
        public int Usuario_ID { get; set; }
        public bool Visible { get; set; }
        #endregion

        #region "Constructor"
        public ConfiguracionModel()
        {
            Configuracion_ID = -1;
            Nombre = string.Empty;
            Servidor_SMTP = string.Empty;
            Puerto = -1;
            Usuario = string.Empty;
            Contraseña = string.Empty;
            Correo_De = string.Empty;
            Nombre_Mostrar = string.Empty;
            Correo_Para = string.Empty;
            Correo_CC = string.Empty;
            Asunto = string.Empty;
            Titulo_Correo = string.Empty;
            Formato_Html = false;
            Activo = false;
            Orden = -1;
            Fecha_Creacion = new DateTime(2000, 1, 1);
            Fecha_Actualizacion = new DateTime(2000, 1, 1);
            Usuario_ID = -1;
            Visible = false;
        }
        #endregion
    }
}
