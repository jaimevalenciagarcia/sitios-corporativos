﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COTEL.Models
{
    public class VentaModel
    {
        #region "Constantes"
        public struct SPParamNames
        {
            public string Venta_ID { get { return ("@Venta_ID"); } }
            public string Cliente_ID { get { return ("@Cliente_ID"); } }
            public string Tarjeta_ID { get { return ("@Tarjeta_ID"); } }
            public string Monto { get { return ("@Monto"); } }
            public string Activo { get { return ("@Activo"); } }
            public string Orden { get { return ("@Orden"); } }
            public string Fecha_Creacion { get { return ("@Fecha_Creacion"); } }
            public string Fecha_Actualizacion { get { return ("@Fecha_Actualizacion"); } }
            public string Usuario_ID { get { return ("@Usuario_ID"); } }
            public string Visible { get { return ("@Visible"); } }
        }
        public struct SPNames
        {
            public string Insert { get { return ("SP_InsertaVentas"); } }
        }

        public SPParamNames ParamNames;
        public SPNames StoredProcNames;
        public enum FieldNames { Venta_ID, Cliente_ID, Tarjeta_ID, Monto, Activo, Orden, Fecha_Creacion, Fecha_Actualizacion, Usuario_ID, Visible };

        #endregion

        #region "Propiedades"
        public int Venta_ID { get; set; }
        public int Cliente_ID { get; set; }
        public int Tarjeta_ID { get; set; }
        public decimal Monto { get; set; }
        public bool Activo { get; set; }
        public int Orden { get; set; }
        public DateTime Fecha_Creacion { get; set; }
        public DateTime Fecha_Actualizacion { get; set; }
        public int Usuario_ID { get; set; }
        public bool Visible { get; set; }
        #endregion

        #region "Constructor"
        public VentaModel()
        {
            Venta_ID = -1;
            Cliente_ID = -1;
            Tarjeta_ID = -1;
            Monto = -1;
            Activo = false;
            Orden = -1;
            Fecha_Creacion = new DateTime(2000, 1, 1);
            Fecha_Actualizacion = new DateTime(2000, 1, 1);
            Usuario_ID = -1;
            Visible = false;
        }
        #endregion
    }
}