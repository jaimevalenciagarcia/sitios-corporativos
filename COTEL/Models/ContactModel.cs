﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace COTEL.Classes
{
    public class ContactModel
    {
        public string Titulo_Pagina { get; set; }
        public string Titulo_Encabezado { get; set; }
        public string SubTitulo_Encabezado { get; set; }

        public string Titulo_Cuerpo_Mapa { get; set; }
        public string Texto_Cuerpo_Mapa { get; set; }
        public string URL_Imagen_Mapa { get; set; }
        public string URL_Ubicacion_Mapa { get; set; }

        public string Titulo_Cuerpo_Datos { get; set; }
        public string Texto_Cuerpo_Datos { get; set; }

        public List<ContactDataBody> ContactDataBody { get; set; }

        public string Titulo_Cuerpo_Mensaje { get; set; }
        public string Texto_Cuerpo_Mensaje { get; set; }


        [Required(ErrorMessage = "El campo es requerido")]
        public string Nombre_Contacto { get; set; }

        [Required(ErrorMessage = "El campo es requerido")]
        public string Email_Contacto { get; set; }

        [Required(ErrorMessage = "El campo es requerido")]
        public string Numero_Contacto { get; set; }

        [Required(ErrorMessage = "El campo es requerido")]
        public string Mensaje_Contacto { get; set; }


        public string Enviar_Contacto { get; set; }

        public LayoutDataModel LayoutData { get; set; }
    }
}