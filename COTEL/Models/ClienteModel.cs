﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COTEL.Models
{
    public class ClienteModel
    {
        #region "Constantes"
        public struct SPParamNames
        {
            public string Cliente_ID { get { return ("@Cliente_ID"); } }
            public string Nombre { get { return ("@Nombre"); } }
            public string EMail { get { return ("@EMail"); } }
            public string NIP_Actual { get { return ("@NIP_Actual"); } }
            public string Activo { get { return ("@Activo"); } }
            public string Orden { get { return ("@Orden"); } }
            public string Fecha_Creacion { get { return ("@Fecha_Creacion"); } }
            public string Fecha_Actualizacion { get { return ("@Fecha_Actualizacion"); } }
            public string Usuario_ID { get { return ("@Usuario_ID"); } }
            public string Visible { get { return ("@Visible"); } }
        }
        public struct SPNames
        {
            public string Insert { get { return ("SP_InsertaCatClientes"); } }
        }

        public SPParamNames ParamNames;
        public SPNames StoredProcNames;
        public enum FieldNames { Cliente_ID, Nombre, EMail, NIP_Actual, Activo, Orden, Fecha_Creacion, Fecha_Actualizacion, Usuario_ID, Visible };

        #endregion

        #region "Propiedades"
        public int Cliente_ID { get; set; }
        public string Nombre { get; set; }
        public string EMail { get; set; }
        public string NIP_Actual { get; set; }
        public bool Activo { get; set; }
        public int Orden { get; set; }
        public DateTime Fecha_Creacion { get; set; }
        public DateTime Fecha_Actualizacion { get; set; }
        public int Usuario_ID { get; set; }
        public bool Visible { get; set; }
        #endregion

        #region "Constructor"
        public ClienteModel()
        {
            Cliente_ID = -1;
            Nombre = string.Empty;
            EMail = string.Empty;
            NIP_Actual = string.Empty;
            Activo = false;
            Orden = -1;
            Fecha_Creacion = new DateTime(2000, 1, 1);
            Fecha_Actualizacion = new DateTime(2000, 1, 1);
            Usuario_ID = -1;
            Visible = false;
        }
        #endregion
    }
}