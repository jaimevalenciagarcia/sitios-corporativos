﻿// Clase para la capa de Modelos de la tabla Cat_Compañias
// Fecha de creación viernes, 27 de marzo de 2015 12:48:05 p. m.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COTEL.Models
{
    public class CompañiaModel
    {
        #region "Constantes"
        public struct SPParamNames
        {
            public string Compañia_ID { get { return ("@Compañia_ID"); } }
            public string RFC { get { return ("@RFC"); } }
            public string Nombre_Comercial { get { return ("@Nombre_Comercial"); } }
            public string Razon_Social { get { return ("@Razon_Social"); } }
            public string Direccion { get { return ("@Direccion"); } }
            public string Telefono { get { return ("@Telefono"); } }
            public string Email { get { return ("@Email"); } }
            public string Sitio_Web { get { return ("@Sitio_Web"); } }
            public string Tema_ID { get { return ("@Tema_ID"); } }
            public string Configuracion_ID { get { return ("@Configuracion_ID"); } }
            public string Activo { get { return ("@Activo"); } }
            public string Orden { get { return ("@Orden"); } }
            public string Fecha_Creacion { get { return ("@Fecha_Creacion"); } }
            public string Fecha_Actualizacion { get { return ("@Fecha_Actualizacion"); } }
            public string Usuario_ID { get { return ("@Usuario_ID"); } }
            public string Visible { get { return ("@Visible"); } }
        }
        public struct SPNames
        {
            public string Insert { get { return ("SP_InsertaCatCompañias"); } }
        }

        public SPParamNames ParamNames;
        public SPNames StoredProcNames;
        public enum FieldNames { Compañia_ID, RFC, Nombre_Comercial, Razon_Social, Direccion, Telefono, Email, Sitio_Web, Tema_ID, Configuracion_ID, Activo, Orden, Fecha_Creacion, Fecha_Actualizacion, Usuario_ID, Visible };

        #endregion

        #region "Propiedades"
        public int Compañia_ID { get; set; }
        public string RFC { get; set; }
        public string Nombre_Comercial { get; set; }
        public string Razon_Social { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public string Email { get; set; }
        public string Sitio_Web { get; set; }
        public int Tema_ID { get; set; }
        public int Configuracion_ID { get; set; }
        public bool Activo { get; set; }
        public int Orden { get; set; }
        public DateTime Fecha_Creacion { get; set; }
        public DateTime Fecha_Actualizacion { get; set; }
        public int Usuario_ID { get; set; }
        public bool Visible { get; set; }
        #endregion

        #region "Constructor"
        public CompañiaModel()
        {
            Compañia_ID = -1;
            RFC = string.Empty;
            Nombre_Comercial = string.Empty;
            Razon_Social = string.Empty;
            Direccion = string.Empty;
            Telefono = string.Empty;
            Email = string.Empty;
            Sitio_Web = string.Empty;
            Tema_ID = -1;
            Configuracion_ID = -1;
            Activo = false;
            Orden = -1;
            Fecha_Creacion = new DateTime(2000, 1, 1);
            Fecha_Actualizacion = new DateTime(2000, 1, 1);
            Usuario_ID = -1;
            Visible = false;
        }
        #endregion
    }
}
