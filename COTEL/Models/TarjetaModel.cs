﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COTEL.Models
{
    public class TarjetaModel
    {
        #region "Constantes"
        public struct SPParamNames
        {
            public string Tarjeta_ID { get { return ("@Tarjeta_ID"); } }
            public string Numero { get { return ("@Numero"); } }
            public string Fecha_Ultimo_Uso { get { return ("@Fecha_Ultimo_Uso"); } }
            public string Activo { get { return ("@Activo"); } }
            public string Orden { get { return ("@Orden"); } }
            public string Fecha_Creacion { get { return ("@Fecha_Creacion"); } }
            public string Fecha_Actualizacion { get { return ("@Fecha_Actualizacion"); } }
            public string Usuario_ID { get { return ("@Usuario_ID"); } }
            public string Visible { get { return ("@Visible"); } }
        }
        public struct SPNames
        {
            public string Insert { get { return ("SP_InsertaCatTarjetas"); } }
        }

        public SPParamNames ParamNames;
        public SPNames StoredProcNames;
        public enum FieldNames { Tarjeta_ID, Numero, Fecha_Ultimo_Uso, Activo, Orden, Fecha_Creacion, Fecha_Actualizacion, Usuario_ID, Visible };

        #endregion

        #region "Propiedades"
        public int Tarjeta_ID { get; set; }
        public String Numero { get; set; }
        public DateTime Fecha_Ultimo_Uso { get; set; }
        public bool Activo { get; set; }
        public int Orden { get; set; }
        public DateTime Fecha_Creacion { get; set; }
        public DateTime Fecha_Actualizacion { get; set; }
        public int Usuario_ID { get; set; }
        public bool Visible { get; set; }
        #endregion

        #region "Constructor"
        public TarjetaModel()
        {
            Tarjeta_ID = -1;
            Numero = string.Empty;
            Fecha_Ultimo_Uso = new DateTime(2000, 1, 1);
            Activo = false;
            Orden = -1;
            Fecha_Creacion = new DateTime(2000, 1, 1);
            Fecha_Actualizacion = new DateTime(2000, 1, 1);
            Usuario_ID = -1;
            Visible = false;
        }
        #endregion
    }
}