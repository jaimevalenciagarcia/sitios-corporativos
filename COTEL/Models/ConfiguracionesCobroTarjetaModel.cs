﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COTEL.Models
{
    public class ConfiguracionesCobroTarjetaModel
    {
        #region "Constantes"
		public struct SPParamNames
		{
			public string Configuracion_ID {get{return("@Configuracion_ID");}}
			public string Monto_Maximo {get{return("@Monto_Maximo");}}
			public string Maximo_Veces_Recarga {get{return("@Maximo_Veces_Recarga");}}
			public string Activo {get{return("@Activo");}}
			public string Orden {get{return("@Orden");}}
			public string Fecha_Creacion {get{return("@Fecha_Creacion");}}
			public string Fecha_Actualizacion {get{return("@Fecha_Actualizacion");}}
			public string Usuario_ID {get{return("@Usuario_ID");}}
			public string Visible {get{return("@Visible");}}
		}
		public struct SPNames
		{
			public string Insert { get { return ("SP_InsertaConfiguraciones"); } }
		}

		public SPParamNames ParamNames;
		public SPNames StoredProcNames;
		public enum FieldNames { Configuracion_ID, Monto_Maximo, Maximo_Veces_Recarga, Activo, Orden, Fecha_Creacion, Fecha_Actualizacion, Usuario_ID, Visible };

		#endregion

		#region "Propiedades"
		public int Configuracion_ID { get; set; }
		public decimal Monto_Maximo { get; set; }
		public Int16 Maximo_Veces_Recarga { get; set; }
		public bool Activo { get; set; }
		public int Orden { get; set; }
		public DateTime Fecha_Creacion { get; set; }
		public DateTime Fecha_Actualizacion { get; set; }
		public int Usuario_ID { get; set; }
		public bool Visible { get; set; }
		#endregion 

		#region "Constructor"
        public ConfiguracionesCobroTarjetaModel()
		{
			Configuracion_ID = -1;
			Monto_Maximo = -1;
			Maximo_Veces_Recarga = -1;
			Activo = false;
			Orden = -1;
			Fecha_Creacion = new DateTime(2000, 1, 1);
			Fecha_Actualizacion = new DateTime(2000, 1, 1);
			Usuario_ID = -1;
			Visible = false;
		}
		#endregion
    }
}