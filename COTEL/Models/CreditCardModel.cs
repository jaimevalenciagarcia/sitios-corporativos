﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace COTEL.Models
{
    public class CreditCardModel
    {
        [Required(ErrorMessage = "No se ha especificado el nombre del Tarjetahabiente")]
        public string TarjetaHabiente { get; set; }

        [Required(ErrorMessage = "No se ha especificado el número de tarjeta de crédito")]
        [RegularExpression(@"^\d+$", ErrorMessage = "Numero tarjeta inválido")]
        public string NumeroTarjeta { get; set; }

        [Required(ErrorMessage = "Requerido")]
        [RegularExpression(@"(^\d+$)", ErrorMessage = "Numero CVC inválido")]
        [StringLength(4, ErrorMessage = "El número no puede contener más de 4 caracteres.")]
        public string CVC { get; set;}

        [Required(ErrorMessage = "Requerido.")]
        [RegularExpression(@"(0[123456789]|10|11|12)", ErrorMessage = "Mes Inválido")]
        [StringLength(2, ErrorMessage = "El número no puede contener más de 2 caracteres.")]
        [Range(1, 12, ErrorMessage = "Mes Inválido.")]
        public string ExpiracionMes { get; set; }


        [Required(ErrorMessage = "Requerido.")]
        [RegularExpression(@"(([1][9][0-9][0-9])|([2][0-9][0-9][0-9]))", ErrorMessage = "Año Inválido")]
        [StringLength(4, ErrorMessage = "El número no puede contener más de 4 caracteres.")]
        [Range(2015, 2070, ErrorMessage = "Año Inválido.")]
        public string ExpiracionAño { get; set; }

        [Required(ErrorMessage = "Requerido")]
        [RegularExpression(@"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$", ErrorMessage = "EMail Inválido")]
        public string CorreoElectronico { get; set; }

        public CreditCardModel()
        {
            TarjetaHabiente = string.Empty;
            NumeroTarjeta = string.Empty;
            CVC = string.Empty;
            ExpiracionMes = string.Empty;
            ExpiracionAño = string.Empty;
            CorreoElectronico = string.Empty;
        }
    }
}