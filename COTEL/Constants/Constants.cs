﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COTEL.Constants
{
    public class Constants
    {
        public static string RFC = "COTEL";

        public static string Name_Procedure = "sp_ConsultaDatos";

        public static string Name_ParameterCompany = "Compañia_ID";
        public static string Name_ParameterModule = "Modulo_ID";
        public static string Name_ParameterLanguage = "Idioma_ID";
        public static string Name_ParameterListable = "Listable";
        public static string Name_ParameterGrupal = "Grupal";
        public static string Name_ParameterNameConcept = "Nombre_Concepto";

        public static string Module_Index = "Index";
        public static string Module_LayoutData = "Datos Generales";
        public static string Module_Product = "Productos";
        public static string Module_Project = "Proyectos";
        public static string Module_Contact = "Contacto";
    }
}