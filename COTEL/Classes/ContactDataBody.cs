﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COTEL.Classes
{
    public class ContactDataBody
    {
        public string Titulo { get; set; }

        public List<ContactDataBodyDetail> ContactDataBodyDetail { get; set; }

    }
}