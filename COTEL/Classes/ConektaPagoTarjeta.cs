﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COTEL.Classes
{
    public class ConektaPagoTarjeta
    {
        public string description { get; set; }
        public int amount { get; set; }
        public string currency { get; set; }
        public string reference_id { get; set; }
        public string card { get; set; }
        public List<ConektaDetalles> details {get; set;}

        public ConektaPagoTarjeta()
        {
            description = "TAE";
            amount = 0;
            currency = "MXN";
            reference_id = string.Empty;
            card = string.Empty;
            details = new List<ConektaDetalles>();
            //details = new List<ConektaItem>();
        }

    }

}