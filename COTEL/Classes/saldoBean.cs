﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COTEL.Classes
{
    /// <comentarios/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class saldoBean
    {
        [System.Xml.Serialization.XmlElement("dsaldo")]
        public decimal dsaldo {get; set;}
        
    }
}




