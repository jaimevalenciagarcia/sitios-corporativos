﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COTEL.Classes
{
    public class ConektaItem
    {
        public string name { get; set; }
        public string sku { get; set; }
        public int unit_price { get; set; }
        public string description { get; set; }
        public int quantity { get; set; }
        public string type { get; set; }

        public ConektaItem()
        {
            name = string.Empty;
            sku = string.Empty;
            unit_price = 0;
            description = string.Empty;
            quantity = 1;
            type = string.Empty;
        }
    }
}