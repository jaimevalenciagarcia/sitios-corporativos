﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COTEL.Classes
{

    /// <comentarios/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class responseBean
    {
        [System.Xml.Serialization.XmlElement("c_response")]
        public byte c_response { get; set; }

        [System.Xml.Serialization.XmlElement("s_response")]
        public string s_response { get; set; }
    }


}

