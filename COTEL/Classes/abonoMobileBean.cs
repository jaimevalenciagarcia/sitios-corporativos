﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COTEL.Classes
{

    /// <comentarios/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class abonoMobileBean
    {
        [System.Xml.Serialization.XmlElement("dfechahora")]
        public System.DateTime dfechahora{ get; set; }
        
        [System.Xml.Serialization.XmlElement("srespuestatexto")]
        public string srespuestatexto { get; set; }

        [System.Xml.Serialization.XmlElement("ufoliocotel")]
        public uint ufoliocotel { get; set; }

        [System.Xml.Serialization.XmlElement("ufoliotelcel")]
        public uint ufoliotelcel { get; set; }

        [System.Xml.Serialization.XmlElement("umonto")]
        public byte umonto { get; set; }

        [System.Xml.Serialization.XmlElement("umontoabonado")]
        public byte umontoabonado { get; set; }

        [System.Xml.Serialization.XmlElement("urespuesta")]
        public byte urespuesta { get; set; }

        [System.Xml.Serialization.XmlElement("utelefono")]
        public ulong utelefono { get; set; }
    }
}

