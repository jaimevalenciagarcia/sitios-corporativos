﻿using COTEL.DataAccess;
using COTEL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace COTEL.Classes
{
    public class ValidacionesSistema
    {
        public static string msConnectionString = WebConfigurationManager.ConnectionStrings["SitiosConn"].ConnectionString;
        private const string DBKEY = "AERus76iTCtMdF15";

        /// <summary>
        /// Busca en la base de datos si la tarjeta de crédito existe
        /// </summary>
        /// <param name="lsCreditCard">Número de la tarjeta de crédito</param>
        /// <returns>-1 si la tarjeta no existe o si se encontró SQL Injection, en caso contrario retorna el id de la tarjeta de crédito</returns>
        public int CreditCardExists(string lsCreditCard, string lsKey)
        {
            DATarjeta ldaTarjeta = new DATarjeta(msConnectionString);
            TarjetaModel loTarjeta = new TarjetaModel();
            int liCreditCardID = -1;
            string lsEncryptedCard = string.Empty;

            if (!ValidacionesSistema.SQLInjectionAttempted(lsCreditCard))
            {
                lsEncryptedCard = COTELConsumer.EncriptarExpresion(lsCreditCard, lsKey);
                loTarjeta = ldaTarjeta.DAConsultEntity("NUMERO", lsEncryptedCard);
                liCreditCardID = loTarjeta.Tarjeta_ID;
            }
            return (liCreditCardID);
        }

        /// <summary>
        /// Busca en la base de datos si el cliente existe
        /// </summary>
        /// <param name="lsTarjetaHabiente">Nombre del Tarjetahabiente</param>
        /// <param name="lsCorreoElectronico">Nombre del correo electrónico del tarjetahabiente</param>
        /// <returns></returns>
        internal int ClientExists(string lsTarjetaHabiente, string lsCorreoElectronico)
        {
            //loCreditCard.TarjetaHabiente, loCreditCard.CorreoElectronico
            DAClienteCustom01 ldaCliente = new DAClienteCustom01(msConnectionString);
            ClienteModel loCliente = new ClienteModel();
            int liClienteID = -1;

            if (!ValidacionesSistema.SQLInjectionAttempted(lsTarjetaHabiente) || !ValidacionesSistema.SQLInjectionAttempted(lsCorreoElectronico))
            {
                loCliente  = ldaCliente.DAConsultCliente(lsTarjetaHabiente, lsCorreoElectronico);
                if (loCliente.Cliente_ID != -1)
                { 
                    //El cliente existe, revisar si coincide con el correo electrónico
                    //if (loCliente.EMail != lsCorreoElectronico)
                    //    liClienteID = -1;
                    //else
                    liClienteID = loCliente.Cliente_ID;
                }
                //liCreditCardID = loTarjeta.Tarjeta_ID;
            }
            return (liClienteID);
            
        }

        internal string ObtenerNIPActual(CreditCardModel loCreditCard)
        {
            ClienteModel loModel = new ClienteModel();
            DACliente ldaDataAccess = new DACliente(msConnectionString);
            int liClientID = -1;
            string lsNIPActual = string.Empty;
            liClientID = ClientExists(loCreditCard.TarjetaHabiente, loCreditCard.CorreoElectronico);
            if (liClientID != -1)
            {
                loModel = ldaDataAccess.DAConsultEntity(liClientID);
                lsNIPActual = loModel.NIP_Actual;
            }
            return (lsNIPActual);
        }

        public static DateTime DAOObtenerFechaSistema()
        {
            DateTime FechaSistema;
            DataTable dt = new DataTable();

            SqlConnection loSQLConnection;
            SqlCommand loSQLCommand;

            loSQLConnection = new SqlConnection(msConnectionString);
            loSQLCommand = new SqlCommand("sp_ObtenerFechaSistema", loSQLConnection);
            loSQLCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                loSQLConnection.Open();

                SqlDataReader dr = loSQLCommand.ExecuteReader();
                dt.Load(dr);

                loSQLConnection.Close();
            }
            catch (SqlException sqlEx)
            {
                throw new Exception(sqlEx.Message);
                //errors = sqlEx.Message;
                //return false;
            }

            FechaSistema = Convert.ToDateTime(dt.Rows[0]["FechaSistema"].ToString());

            return (FechaSistema);
        }

        public static bool SQLInjectionAttempted(string lsExpression)
        {
            bool lbInjectionAttempted = true;
            if (lsExpression.Contains("'"))
                return (lbInjectionAttempted);
            if (lsExpression.ToUpper().Contains(" FROM "))
                return (lbInjectionAttempted);
            if (lsExpression.ToUpper().Contains(" INTO "))
                return (lbInjectionAttempted);
            if (lsExpression.ToUpper().Contains("--"))
                return (lbInjectionAttempted);
            if (lsExpression.ToUpper().Contains("/*"))
                return (lbInjectionAttempted);
            if (lsExpression.ToUpper().Contains("*/"))
                return (lbInjectionAttempted);
            if (lsExpression.ToUpper().Contains(" EXEC "))
                return (lbInjectionAttempted);
            if (lsExpression.ToUpper().Contains(" DELETE "))
                return (lbInjectionAttempted);
            if (lsExpression.ToUpper().Contains(" INSERT "))
                return (lbInjectionAttempted);
            if (lsExpression.ToUpper().Contains(" INSERT "))
                return (lbInjectionAttempted);
            if (lsExpression.ToUpper().Contains(" 1=1 "))
                return (lbInjectionAttempted);
            if (lsExpression.ToUpper().Contains(" 1 =1 "))
                return (lbInjectionAttempted);
            if (lsExpression.ToUpper().Contains(" 1= 1 "))
                return (lbInjectionAttempted);
            if (lsExpression.ToUpper().Contains(" 1 = 1 "))
                return (lbInjectionAttempted);
            lbInjectionAttempted = false;
            return (lbInjectionAttempted);
        }
        
        /// <summary>
        ///  Obtiene el total de ventas de un cliente en las últimas 24 hrs.
        /// </summary>
        /// <param name="liClientID">ID del cliente</param>
        /// <param name="liTarjetaID">ID de la tarjeta</param>
        /// <param name="ldteNow">Fecha y hora actual</param>
        /// <returns></returns>
        public decimal ObtenerTotalVentas(int liClientID, int liTarjetaID, DateTime ldteNow)
        {
            //throw new NotImplementedException();
            decimal ldecTotalVentas = 0; 
            DataTable dt = new DataTable();

            SqlConnection loSQLConnection;
            SqlCommand loSQLCommand;
            SqlParameter loParameter;

            loSQLConnection = new SqlConnection(msConnectionString);
            loSQLCommand = new SqlCommand("SP_ObtenerTotalVentas", loSQLConnection);

            loParameter = new SqlParameter("Tarjeta_ID", liTarjetaID);
            loSQLCommand.Parameters.Add(loParameter);

            loParameter = new SqlParameter("Cliente_ID", liClientID);
            loSQLCommand.Parameters.Add(loParameter);

            loParameter = new SqlParameter("Fecha_Actual", ldteNow);
            loSQLCommand.Parameters.Add(loParameter);
            
            loSQLCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                loSQLConnection.Open();

                SqlDataReader dr = loSQLCommand.ExecuteReader();
                dt.Load(dr);

                loSQLConnection.Close();
            }
            catch (SqlException sqlEx)
            {
                throw new Exception(sqlEx.Message);
                //errors = sqlEx.Message;
                //return false;
            }

            if (dt.Rows.Count > 0)
                ldecTotalVentas = Convert.ToDecimal(dt.Rows[0]["Total_Ventas"].ToString());
            else
                ldecTotalVentas = 0;

            return (ldecTotalVentas);
        }

        public Int16 ObtenerNumeroRecargas(int liClientID, int liTarjetaID, DateTime ldteNow)
        {
            //.ObtenerNumeroRecargas(liClientID, liTarjetaID, ValidacionesSistema.DAOObtenerFechaSistema());
            //throw new NotImplementedException();
            Int16 liTotalRecargas = 0;
            DataTable dt = new DataTable();

            SqlConnection loSQLConnection;
            SqlCommand loSQLCommand;
            SqlParameter loParameter;

            loSQLConnection = new SqlConnection(msConnectionString);
            loSQLCommand = new SqlCommand("SP_ObtenerTotalRecargas", loSQLConnection);

            loParameter = new SqlParameter("Tarjeta_ID", liTarjetaID);
            loSQLCommand.Parameters.Add(loParameter);

            loParameter = new SqlParameter("Cliente_ID", liClientID);
            loSQLCommand.Parameters.Add(loParameter);

            loParameter = new SqlParameter("Fecha_Actual", ldteNow);
            loSQLCommand.Parameters.Add(loParameter);

            loSQLCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                loSQLConnection.Open();

                SqlDataReader dr = loSQLCommand.ExecuteReader();
                dt.Load(dr);

                loSQLConnection.Close();
            }
            catch (SqlException sqlEx)
            {
                throw new Exception(sqlEx.Message);
                //errors = sqlEx.Message;
                //return false;
            }

            if (dt.Rows.Count > 0)
                liTotalRecargas = Convert.ToInt16(dt.Rows[0]["Total_Recargas"].ToString());
            else
                liTotalRecargas = 0;

            return (liTotalRecargas);
        }



        public void RegistrarVenta(CreditCardModel loCreditCard, int liMontoRecarga)
        {
            VentaModel loModel = new VentaModel();
            DAVenta ldaDataAccess = new DAVenta(msConnectionString);
            
            //ClienteModel loClienteModel = new ClienteModel();
            //DACliente ldaDataAccessCliente = new DACliente(msConnectionString);
            //TarjetaModel loTarjetaModel = new TarjetaModel();
            //DATarjeta ldaDataAccessTarjeta = new DATarjeta(msConnectionString);

            int liClienteID = -1;
            int liTarjetaID = -1;

            liClienteID = ClientExists(loCreditCard.TarjetaHabiente, loCreditCard.CorreoElectronico);
            liTarjetaID = CreditCardExists(loCreditCard.NumeroTarjeta, DBKEY);
            loModel.Cliente_ID = liClienteID;
            loModel.Tarjeta_ID = liTarjetaID;
            loModel.Monto = Convert.ToDecimal(liMontoRecarga);

            loModel.Activo = true;
            loModel.Orden = 1;
            loModel.Usuario_ID = 1;
            loModel.Visible = true;

            ldaDataAccess.DAInsertarModel(loModel);
        }
    }
}