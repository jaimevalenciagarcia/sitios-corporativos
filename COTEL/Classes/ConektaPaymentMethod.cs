﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COTEL.Classes
{
    public class ConektaPaymentMethod
    {
        public string name { get; set; }
        public string exp_month { get; set; }
        public string exp_year { get; set; }
        public string auth_code { get; set; }
        public string last4 { get; set; }
        public string brand { get; set; }
    }
}

//        "name":"Luis Duran",
//        "exp_month":"01",
//        "exp_year":"20",
//        "auth_code":"000000",
//        "object":"card_payment",
//        "last4":"5100",
//        "brand":"mc"},