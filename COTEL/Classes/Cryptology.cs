﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace COTEL.Classes
{
    public class Cryptology
    {
        private const string Salt = "";
        private const int SizeOfBuffer = 1024 * 8;

        public static byte[] EncryptStringToBytes(string plainText, byte[] key, byte[] iv)
        {
            // Check arguments.
            if (plainText == null || plainText.Length <= 0)
            {
                throw new ArgumentNullException("plainText");
            }
            if (key == null || key.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }
            if (iv == null || iv.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }

            byte[] encrypted;
            // Create an RijndaelManaged object
            // with the specified key and IV.
            using (var rijAlg = new RijndaelManaged())
            {
                rijAlg.Key = key;
                for (int i = 0; i <= iv.Count() - 1; i++) iv[i] = 0;

                rijAlg.IV = iv;
                rijAlg.Padding = System.Security.Cryptography.PaddingMode.PKCS7;
                rijAlg.Mode = CipherMode.ECB;


                //rijAlg.BlockSize = 256;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = rijAlg.CreateEncryptor(rijAlg.Key, rijAlg.IV);

                // Create the streams used for encryption.
                using (var msEncrypt = new MemoryStream())
                {
                    using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (var swEncrypt = new StreamWriter(csEncrypt))
                        {
                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }


            // Return the encrypted bytes from the memory stream.
            return encrypted;

        }

        public static string DecryptStringFromBytes(byte[] cipherText, byte[] key, byte[] iv)
        {
            // Check arguments.
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (key == null || key.Length <= 0)
                throw new ArgumentNullException("key");
            if (iv == null || iv.Length <= 0)
                throw new ArgumentNullException("key");

            // Declare the string used to hold
            // the decrypted text.
            string plaintext;

            // Create an RijndaelManaged object
            // with the specified key and IV.
            using (var rijAlg = new RijndaelManaged())
            {
                rijAlg.Key = key;
                for (int i = 0; i <= 15; i++) iv[i] = 0;

                rijAlg.IV = iv;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);

                // Create the streams used for decryption.
                using (var msDecrypt = new MemoryStream(cipherText))
                {
                    using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (var srDecrypt = new StreamReader(csDecrypt))
                        {
                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }

            }
            return plaintext;
        }


        public static string Decrypt(String text, String key)
        {
            //decode cipher text from base64
            byte[] cipher = Convert.FromBase64String(text);
            //get key bytes
            byte[] btkey = Encoding.ASCII.GetBytes(key);

            //init AES 128
            RijndaelManaged aes128 = new RijndaelManaged();
            aes128.Mode = CipherMode.ECB;
            aes128.Padding = PaddingMode.Zeros;

            //decrypt
            ICryptoTransform decryptor = aes128.CreateDecryptor(btkey, null);
            MemoryStream ms = new MemoryStream(cipher);
            CryptoStream cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Read);

            byte[] plain = new byte[cipher.Length];
            int decryptcount = cs.Read(plain, 0, plain.Length);

            ms.Close();
            cs.Close();

            //return plaintext in String
            return Encoding.UTF8.GetString(plain, 0, decryptcount);
        }


        //public static string Desencriptar(string strExpression)//strADesencriptar
        //{
        //    //Dim resultado As String = Nothing
        //    string resultado = string.Empty;
        //    try
        //    {
        //        RijndaelManaged algoritmo = new RijndaelManaged();
        //        byte[] IVAux = new byte[algoritmo.IV.Length - 1];
        //        byte[] bytesADesencriptarConIV = Convert.FromBase64String(strExpression);
        //        byte[] bytesADesencriptar = new byte[bytesADesencriptarConIV.Length - algoritmo.IV.Length - 1];

        //        //Array.Copy(bytesADesencriptarConIV, 0, bytesADesencriptar, 0, bytesADesencriptarConIV.Length - algoritmo.IV.Length);
        //        for (int i = 0; i <= bytesADesencriptarConIV.Count() - 1; i++) bytesADesencriptar[i] = bytesADesencriptarConIV[i];
        //        byte[] bytesDesencriptados;

        //        //inicializar algoritmo
        //        for (int i = 0; i <= bytesADesencriptarConIV.Count() - 1; i++) bytesADesencriptar[i] = bytesADesencriptarConIV[i];

        //        algoritmo.IV = IVAux;
        //        //Encriptar
        //        bytesDesencriptados = algoritmo.CreateDecryptor().TransformFinalBlock(bytesADesencriptar, 0, bytesADesencriptar.Length);

        //        //Convertir bytesEncriptados en un string de resultado
        //        resultado = Encoding.UTF8.GetString(bytesDesencriptados);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception("Error al encriptar cadena de caracteres " + ex.Message);
        //    }

        //    return (resultado);





        //}

        public static void EncryptFile(string inputPath, string outputPath, string password)
        {
            var input = new FileStream(inputPath, FileMode.Open, FileAccess.Read);
            var output = new FileStream(outputPath, FileMode.OpenOrCreate, FileAccess.Write);

            // Essentially, if you want to use RijndaelManaged as AES you need to make sure that:
            // 1.The block size is set to 128 bits
            // 2.You are not using CFB mode, or if you are the feedback size is also 128 bits

            var algorithm = new RijndaelManaged { KeySize = 256, BlockSize = 128 };
            var key = new Rfc2898DeriveBytes(password, Encoding.ASCII.GetBytes(Salt));

            algorithm.Key = key.GetBytes(algorithm.KeySize / 8);
            algorithm.IV = key.GetBytes(algorithm.BlockSize / 8);

            using (var encryptedStream = new CryptoStream(output, algorithm.CreateEncryptor(), CryptoStreamMode.Write))
            {
                CopyStream(input, encryptedStream);
            }
        }

        public static void DecryptFile(string inputPath, string outputPath, string password)
        {
            var input = new FileStream(inputPath, FileMode.Open, FileAccess.Read);
            var output = new FileStream(outputPath, FileMode.OpenOrCreate, FileAccess.Write);

            // Essentially, if you want to use RijndaelManaged as AES you need to make sure that:
            // 1.The block size is set to 128 bits
            // 2.You are not using CFB mode, or if you are the feedback size is also 128 bits
            var algorithm = new RijndaelManaged { KeySize = 256, BlockSize = 128 };
            var key = new Rfc2898DeriveBytes(password, Encoding.ASCII.GetBytes(Salt));

            algorithm.Key = key.GetBytes(algorithm.KeySize / 8);
            algorithm.IV = key.GetBytes(algorithm.BlockSize / 8);

            try
            {
                using (var decryptedStream = new CryptoStream(output, algorithm.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    CopyStream(input, decryptedStream);
                }
            }
            catch (CryptographicException)
            {
                throw new InvalidDataException("Please suppy a correct password");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public static void CopyStream(Stream input, Stream output)
        {
            using (output)
            using (input)
            {
                byte[] buffer = new byte[SizeOfBuffer];
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    output.Write(buffer, 0, read);
                }
            }
        }


    }
}