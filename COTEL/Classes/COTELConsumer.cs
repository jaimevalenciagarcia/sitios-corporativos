﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using System.Diagnostics;
using System.Text;
using System.Security.Cryptography;
using System.Xml.Serialization;
using System.IO;
using COTEL.Models;

namespace COTEL.Classes
{
    public class COTELConsumer
    {
        public const string CURRENT_IP = "201.122.220.18";
        public const string SERVICE_GET_SALDO = "TAEServices2.0.6/resources/saldo";
        public const string SERVICE_ABONAR = "TAEServices2.0.6/resources/abono";
        public const string CURRENT_KEY = "m3x1c02013a2c4e5";

        public const string FIRST_URL_PARAMETER = "tsolicitud=C02G8416DRJM&cn=&rsolicitud=";
        public const string XML_GET_SALDO = "<?xml version='1.0' encoding='UTF-8'?><Solicitud kidentificador='426' strlogin='jpoumian' spassword='B1tware14' strmacadress='MBL9CAL0-C0C8-4H9C-ABG3-3246CH'></Solicitud>";

        public const bool SANDBOX_MODE = true;
        public static bool SaldoSuficiente(decimal ldecCantidadPedida)
        {
            string lsXMLSaldoInfo = string.Empty;
            decimal ldecSaldo = 0;
            bool lbSaldoSuficiente = false;

            var loTask = ConsumeCOTELSaldoSuficiente();
            while (!loTask.IsCompleted)
            {
                Debug.Print("Not Completed!!: " + loTask.IsCompleted.ToString());
            }
            
            var serializer = new XmlSerializer(typeof(responseBean));
            responseBean result;

            using (TextReader reader = new StringReader(loTask.Result))
            {
                result = (responseBean)serializer.Deserialize(reader);
                lsXMLSaldoInfo = result.s_response;
                lsXMLSaldoInfo = DesencriptarExpresion(lsXMLSaldoInfo);


                lsXMLSaldoInfo = lsXMLSaldoInfo.Replace("\r\n", "");
                string[] lsXMLInfo = lsXMLSaldoInfo.Split(new string[] { "<dsaldo>" }, StringSplitOptions.None);
                lsXMLInfo = lsXMLInfo[1].Split(new string[] { "</dsaldo>" }, StringSplitOptions.None);
                ldecSaldo = Convert.ToDecimal(lsXMLInfo[0]);
            }
            if (ldecSaldo >= ldecCantidadPedida)
                lbSaldoSuficiente = true;
            
            return (lbSaldoSuficiente);
        }

        internal static RecargaTelcelModel AplicarRecarga(string lsNoCelular, int liMontoRecarga)
        {
            RecargaTelcelModel loRecargaTelcel = new  RecargaTelcelModel();
            string lsXMLSaldoInfo = string.Empty;
            string lsErrMessage = string.Empty;
            System.Threading.Tasks.Task<string> loTask;
            string lsResults = string.Empty;
            
            if(!SANDBOX_MODE)
            {
                loTask = ConsumeCOTELAplicarRecarga(lsNoCelular, liMontoRecarga);
                while (!loTask.IsCompleted)
                {
                    Debug.Print("Not Completed!!: " + loTask.IsCompleted.ToString());
                }
                Debug.Print("");
                lsResults = loTask.Result; 
            }
            else
            {
                lsResults = "<?xml version=" + (char)34 + "1.0" + (char)34 + " encoding=" + (char)34 + "UTF-8" + (char)34 + " standalone=" + (char)34 + "yes" + (char)34 + "?><responseBean><c_response>1</c_response><s_response>J/U8UqB4gDnc3YyJPaGsWMaglsUdREbfOdVg6gDsF2Gi75vvNNPo/ujnJpntftGRrXOlsBTafBrs&#xD;\r\n/Q+trGJPO9kgGqxBIkNp4RhAUOB5n3uOdymLjhgfMMP8LZnAbtuSjLFIPsZ/tEfbGtZQA/C18bhf&#xD;\r\nY4ZOHv8pp9QK92sn0ZF/+loEh/0cChHGXfOIBXQhpuEgEqkXBMo3H/sEGwOtNTiNkhe5k7RmVNrc&#xD;\r\ncoaKqFRBgCmhG9MPG/TUx+P3ypyuAzvVw8ZG3mMVMgxvPgeyOJ4um7sxa3Kf9yRMZvQRDJ9L9ETU&#xD;\r\n7Y2LQ1eIN73s1d4+vKbC3n4dUjI1uNvg78j1aHMnH9gZITXjgTh3uhmWd7rpd2SRSVoE+VJcoOjU&#xD;\r\n9znjGmRn7LBXitHcSmnXlWgjxEXfYuoIvSqxp+p7TvsSdKzGqPIgU+rDXUOnBLS+XmRP/T2LGlBE&#xD;\r\n5SKHibOudYQ20sLKXryIkSW/4Jd5l0BkIkQPBHpzLVV9h5yiqsG5WApj6d2Po9OqDmfzgS11nzWm&#xD;\r\nkcr0GXFw9PVfgMeV2e9UhyU=</s_response></responseBean>";
            }


            if (lsResults.Contains("error"))
            {
                //Se generó un error en la recarga.
                Debug.Print("");  
            }
            else
            { 
                //Recarga realizada con éxito.
                var serializer = new XmlSerializer(typeof(responseBean));
                responseBean result;

                using (TextReader reader = new StringReader(lsResults))
                {
                    result = (responseBean)serializer.Deserialize(reader);
                    lsXMLSaldoInfo = result.s_response;
                    lsXMLSaldoInfo = DesencriptarExpresion(lsXMLSaldoInfo);
                    lsXMLSaldoInfo = lsXMLSaldoInfo.Replace("\r\n", "").Replace("", "").Replace("    ", "").Replace("\n", "").Replace("", "");


                    loRecargaTelcel.FechaHoraRecarga = Convert.ToString(GetXMLTagInfo("dfechahora", lsXMLSaldoInfo));
                    loRecargaTelcel.FolioCotel = Convert.ToUInt64(GetXMLTagInfo("ufoliocotel", lsXMLSaldoInfo));
                    loRecargaTelcel.FolioTelcel = Convert.ToUInt64(GetXMLTagInfo("ufoliotelcel", lsXMLSaldoInfo));
                    loRecargaTelcel.MontoAbonado = Convert.ToDecimal(GetXMLTagInfo("umontoabonado", lsXMLSaldoInfo));
                    loRecargaTelcel.Telefono = Convert.ToString(GetXMLTagInfo("utelefono", lsXMLSaldoInfo));
                    loRecargaTelcel.TextoRespuesta = Convert.ToString(GetXMLTagInfo("srespuestatexto", lsXMLSaldoInfo));
                        

                    //var xmlAbonoSerializer = new XmlSerializer(typeof(abonoMobileBean));
                    //abonoMobileBean loResponseAbono;
                    //using (TextReader loAbonoReader = new StringReader(lsXMLSaldoInfo))
                    //{
                    //    loResponseAbono = (abonoMobileBean)serializer.Deserialize(loAbonoReader);
                    //    //Obtener información adicional de la recarga.
                    //}
                }
            }

            return (loRecargaTelcel);
        }


        private static string GetXMLTagInfo(string lsTagName, string lsXMLSource)
        {
            string[] lsXMLInfo = lsXMLSource.Split(new string[] { "<" + lsTagName  + ">" }, StringSplitOptions.None);
            lsXMLInfo = lsXMLInfo[1].Split(new string[] { "</" + lsTagName + ">" }, StringSplitOptions.None);
            return(lsXMLInfo[0]);
        }

        private static async System.Threading.Tasks.Task<string> ConsumeCOTELAplicarRecarga(string lsNoCelular, int liMontoRecarga)
        {
            string lsServiceResponse = string.Empty;
            using (var client = new HttpClient())
            {
                HttpResponseMessage response;

                //HTTP GET
                try
                {
                    string lsMessageBody;
                    //Agregar los headers
                    client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Mozilla/5.0");
                    client.DefaultRequestHeaders.TryAddWithoutValidation("Accept-Language", "en-US,en;q=0.5");
                    client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/x-www-form-urlencoded");
                    client.DefaultRequestHeaders.TryAddWithoutValidation("charset", "UTF-8");
                    client.Timeout = new TimeSpan(0, 0, 30);
                    HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, "http://" + CURRENT_IP + "/" + SERVICE_ABONAR);
                    lsMessageBody = FIRST_URL_PARAMETER + EncriptarExpresion("<?xml version='1.0' encoding='UTF-8'?><Solicitud kidentificador='426' strlogin='jpoumian' spassword='B1tware14' strmacadress='MBL9CAL0-C0C8-4H9C-ABG3-3246CH' umonto='" + liMontoRecarga + "' utelefono='" + lsNoCelular + "'></Solicitud>");

                    req.Content = new StringContent(lsMessageBody, Encoding.ASCII);
                    Debug.Print("------------");

                    response = await client.SendAsync(req).ConfigureAwait(false);
                    if (response.IsSuccessStatusCode)
                    {
                        Debug.Print("Exito!!");
                        lsServiceResponse = await response.Content.ReadAsStringAsync();
                    }
                    else
                    {
                        Debug.Print("Error al invocar el servicio");
                        lsServiceResponse = await response.Content.ReadAsStringAsync();
                    }
                }
                catch (HttpRequestException Exception)
                {
                    Console.WriteLine("ERROR");
                    Console.WriteLine(Exception.Message);
                    Console.ReadKey();
                }
            }
            return (lsServiceResponse);
        }

        public static async System.Threading.Tasks.Task<string> ConsumeCOTELSaldoSuficiente()
        {
            string lsServiceResponse = string.Empty;
            using (var client = new HttpClient())
            {

                HttpResponseMessage response;

                //HTTP GET
                try
                {
                    string lsMessageBody;
                    //Agregar los headers
                    client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Mozilla/5.0");
                    client.DefaultRequestHeaders.TryAddWithoutValidation("Accept-Language", "en-US,en;q=0.5");
                    client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/x-www-form-urlencoded");
                    client.DefaultRequestHeaders.TryAddWithoutValidation("charset", "UTF-8");
                    client.Timeout = new TimeSpan(0, 0, 30);
                    HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, "http://" + CURRENT_IP + "/" + SERVICE_GET_SALDO);
                    lsMessageBody = FIRST_URL_PARAMETER + EncriptarExpresion(XML_GET_SALDO);

                    req.Content = new StringContent(lsMessageBody, Encoding.ASCII);
                    Debug.Print("------------");

                    response = await client.SendAsync(req).ConfigureAwait(false);
                    if (response.IsSuccessStatusCode)
                    {
                        Debug.Print("Exito!!");
                        lsServiceResponse = await response.Content.ReadAsStringAsync();
                    }
                    else
                    {
                        Debug.Print("Error al invocar el servicio");
                        lsServiceResponse = await response.Content.ReadAsStringAsync();
                    }
                }
                catch (HttpRequestException Exception)
                {
                    Console.WriteLine("ERROR");
                    Console.WriteLine(Exception.Message);
                    Console.ReadKey();
                }
            }
            return (lsServiceResponse);
        }

        public static string EncriptarExpresion(string lsExpresion, string lsKey = "")
        {
            string lsEncryptedExp = string.Empty;
            byte[] encryptedPassword;

            // Create a new instance of the RijndaelManaged
            // class.  This generates a new key and initialization
            // vector (IV).
            using (var algorithm = new AesManaged())
            {
                algorithm.KeySize = 256; //256;
                algorithm.BlockSize = 128;

                // Encrypt the string to an array of bytes.
                //------------------------------------------------------
                string lsExpToEncrypt = lsExpresion; // "<?xml version='1.0' encoding='UTF-8'?><Solicitud kidentificador='426' strlogin='jpoumian' spassword='B1tware14' strmacadress='MBL9CAL0-C0C8-4H9C-ABG3-3246CH'></Solicitud>";
                Encoding enc = new UTF8Encoding(true, true);
                byte[] strKeyValue = null;
                if (lsKey == string.Empty)
                    strKeyValue = enc.GetBytes(CURRENT_KEY);
                else
                    strKeyValue = enc.GetBytes(lsKey); 
                //-------------------------------------------------------

                encryptedPassword = Cryptology.EncryptStringToBytes(lsExpToEncrypt, strKeyValue, algorithm.IV);
                string lsValue = Convert.ToBase64String(encryptedPassword);
                string lsEncodedValue = HttpUtility.UrlEncode(lsValue, Encoding.UTF8);
                lsEncryptedExp = lsEncodedValue;
            }
            return (lsEncryptedExp);
        }

        public static string DesencriptarExpresion(string lsExpresion, string lsKey = "")
        {
            // Create a new instance of the RijndaelManaged
            // class.  This generates a new key and initialization
            // vector (IV).
            using (var algorithm = new AesManaged())//  Rijndael())//  RijndaelManaged())
            {
                algorithm.KeySize = 256; //256;
                algorithm.BlockSize = 128;

                //// Encrypt the string to an array of bytes.
                ////------------------------------------------------------
                Encoding enc = new UTF8Encoding(true, true);
                byte[] strKeyValue = null;
                if (lsKey == string.Empty)
                    strKeyValue = enc.GetBytes(CURRENT_KEY);
                else
                    strKeyValue = enc.GetBytes(lsKey);

                lsExpresion  = lsExpresion.Replace("\r\n", string.Empty);
                string lsResponse = Cryptology.Decrypt(lsExpresion, CURRENT_KEY);
                lsExpresion = lsResponse;
                
            }
            return (lsExpresion);
        }
    }
}