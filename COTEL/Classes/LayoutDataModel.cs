﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COTEL.Classes
{
    public class LayoutDataModel
    {
        public string Titulo_Sitio { get; set; }
        public string Nombre_Tema { get; set; }
        public string Email { get; set; }
        public string Telefono { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        //public string Google { get; set; }
        //public string Youtube { get; set; }
        //public string Skype { get; set; }

        public string Quienes_Somos_Titulo { get; set; }
        public string Quienes_Somos_Texto { get; set; }
        public string Mision_Titulo { get; set; }
        public string Mision_Texto { get; set; }
        public string Tweet_Titulo { get; set; }
        public string Direccion { get; set; }
        public string Titulo_Proyectos { get; set; }
        public string URL_Proyectos { get; set; }
    }
}