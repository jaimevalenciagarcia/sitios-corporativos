﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COTEL.Classes
{
    public class ConektaPaymentInfo
    {
        public string id { get; set; }
        public bool livemode { get; set; }
        public string created_at { get; set; }
        public string status { get; set; }
        public string currency { get; set; }
        public string description { get; set; }
        public string reference_id { get; set; }
        public string failure_code { get; set; }
        public string failure_message { get; set; }
        public string monthly_installments { get; set; }
        public int amount { get; set; }
        public long paid_at { get; set; }
        public int fee { get; set; }
        public string customer_id { get; set; }
        public ConektaPaymentMethod payment_method { get; set; }

    }
}


//{
//    "id":"555148bf19ce88162e00025d",
//    "livemode":false,
//    "created_at":1431390399,

//    "status":"paid",
//    "currency":"MXN",
//    "description":"TAE",
//    "reference_id":"9839-TAE",
//    "failure_code":null,
//    "failure_message":null,
//    "monthly_installments":null,
//    "object":"charge",
//    "amount":10000,

//    "paid_at":1431390404,

//    "fee":626,
//    "customer_id":"",
//    "refunds":[],
//    "payment_method":{
//        "name":"Luis Duran",
//        "exp_month":"01",
//        "exp_year":"20",
//        "auth_code":"000000",
//        "object":"card_payment",
//        "last4":"5100",
//        "brand":"mc"},
//    "details":{
//        "name":null,
//        "phone":null,
//        "email":null,
//        "line_items":[]
//    }
//}