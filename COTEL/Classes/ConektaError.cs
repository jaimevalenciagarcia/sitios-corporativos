﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COTEL.Classes
{
    public class ConektaError
    {
        public string type { get; set; }
        public string message { get; set; }
        public string message_to_purchaser { get; set; }
        public string param { get; set; }
        public string code { get; set; }
    }
}