﻿using COTEL.DataAccess;
using COTEL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Configuration;
using WebSitesClassLibrary.GeneralPurpose;

namespace COTEL.Classes
{
    public class EmailEngine
    {
        public string LastErrorMessage { get; set; }
        private string msConnectionString = WebConfigurationManager.ConnectionStrings["SitiosConn"].ConnectionString;
        public EmailEngine()
        {
            LastErrorMessage = string.Empty;
        }
        
        public bool SendNIPMailTo(string lsEmail, string lsNIP)
        {
            CompañiaModel mCompany = new CompañiaModel();
            ConfiguracionModel mConfiguration = new ConfiguracionModel();

            Mail clsMail = new Mail();

            bool MailEnviado;
            string sBody;

            DACompañia daCompany = new DACompañia(msConnectionString);
            //DATema daTheme = new DATema(cadena);
            DAConfiguracion daConfiguration = new DAConfiguracion(msConnectionString);

            mCompany = daCompany.DAOConsultCompany(Constants.Constants.RFC);
            //mTheme = daTheme.DAConsultEntity(mCompany.Tema_ID);
            mConfiguration = daConfiguration.DAConsultEntity(mCompany.Configuracion_ID);

            clsMail.ConfigurarMail(mConfiguration.Correo_De, mConfiguration.Nombre_Mostrar, mConfiguration.Usuario, mConfiguration.Contraseña, mConfiguration.Puerto, mConfiguration.Servidor_SMTP);

            sBody = clsMail.FormatBodyTemplateForNIP(lsNIP); //clsMail.FormatBodyTemplateNice(mConfiguration.Titulo_Correo, loModel.Nombre_Contacto, loModel.Email_Contacto, loModel.Numero_Contacto, loModel.Mensaje_Contacto);  //  contact_name, contact_email, contact_number, contact_message);

            MailEnviado = clsMail.EnviarMail(lsEmail, "NIP para continuar con su proceso de compra TAE", sBody, mConfiguration.Formato_Html);


            return (MailEnviado);
        }

        public bool SendRecargaMailTo(string lsEmail, decimal ldecTotalRecarga, string lsNumeroCelular, string lsRespuestaTELCEL, string lsFolioCOTEL, string  lsFolioTELCEL, string lsFolioCONEKTA)
        {
            CompañiaModel mCompany = new CompañiaModel();
            ConfiguracionModel mConfiguration = new ConfiguracionModel();

            Mail clsMail = new Mail();

            bool MailEnviado;
            string sBody;

            DACompañia daCompany = new DACompañia(msConnectionString);
            //DATema daTheme = new DATema(cadena);
            DAConfiguracion daConfiguration = new DAConfiguracion(msConnectionString);

            mCompany = daCompany.DAOConsultCompany(Constants.Constants.RFC);
            //mTheme = daTheme.DAConsultEntity(mCompany.Tema_ID);
            mConfiguration = daConfiguration.DAConsultEntity(mCompany.Configuracion_ID);

            clsMail.ConfigurarMail(mConfiguration.Correo_De, mConfiguration.Nombre_Mostrar, mConfiguration.Usuario, mConfiguration.Contraseña, mConfiguration.Puerto, mConfiguration.Servidor_SMTP);

            sBody = clsMail.FormatBodyTemplateForRecarga(ldecTotalRecarga, lsNumeroCelular, lsRespuestaTELCEL, lsFolioCOTEL, lsFolioTELCEL, lsFolioCONEKTA); //clsMail.FormatBodyTemplateNice(mConfiguration.Titulo_Correo, loModel.Nombre_Contacto, loModel.Email_Contacto, loModel.Numero_Contacto, loModel.Mensaje_Contacto);  //  contact_name, contact_email, contact_number, contact_message);

            MailEnviado = clsMail.EnviarMail(lsEmail, "Compra de TAE", sBody, mConfiguration.Formato_Html);


            return (MailEnviado);
        }


    }
}
