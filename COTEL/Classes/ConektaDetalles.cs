﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COTEL.Classes
{
    public class ConektaDetalles
    {
        public string name { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public List<ConektaItem> line_items { get; set; }

        public ConektaDetalles()
        {
            name = string.Empty;
            email = string.Empty;
            phone = string.Empty;
            line_items = new List<ConektaItem>();
        }
    }
}