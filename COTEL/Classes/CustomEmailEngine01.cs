﻿using COTEL.DataAccess;
using COTEL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using WebSitesClassLibrary.DataAccess;

namespace COTEL.Classes
{
    public class CustomEmailEngine01 : EmailEngine
    {
        private string msConnectionString = WebConfigurationManager.ConnectionStrings["SitiosConn"].ConnectionString;
        private const string DBKEY = "AERus76iTCtMdF15";
        public bool NIPSentTo(CreditCardModel loCreditCard)
        {
            ValidacionesSistema loValidaciones = new ValidacionesSistema();
            
            bool lbMailSent = false;
            int liTarjetaID = -1;
            int liClientID = -1;
            Int16 liTotalRecargas = 0;
            string lsNIP = string.Empty;
            decimal ldecTotalVentas = 0;
            bool lbNewClient = false;

            ConfiguracionesCobroTarjetaModel loConfiguraciones = new ConfiguracionesCobroTarjetaModel();
            DAConfiguracionCobroTarjeta ldaConfiguraciones = new DAConfiguracionCobroTarjeta(msConnectionString);
            CaptchaImage loCaptchaHandler;

            LastErrorMessage = string.Empty;
            liTarjetaID = loValidaciones.CreditCardExists(loCreditCard.NumeroTarjeta, DBKEY);
            //CaptchaImage loCaptchaHandler;
            if (liTarjetaID == -1)
            {
                //La tarjeta de crédito no existe, registrar en base de datos
                liTarjetaID = InsertNewCreditCard(loCreditCard.NumeroTarjeta);
            }
            liClientID = loValidaciones.ClientExists(loCreditCard.TarjetaHabiente, loCreditCard.CorreoElectronico);
            if (liClientID == -1)
            {
                //Cliente no existe, darlo de alta
                liClientID = InsertNewClient(loCreditCard.TarjetaHabiente, loCreditCard.CorreoElectronico);
                lbNewClient = true;
            }

            //Obtener las configuraciones del sistema del cobro con tarjeta.
            loConfiguraciones = ldaConfiguraciones.DAConsultEntity();
            
            //Obtener el total de ventas
            ldecTotalVentas = loValidaciones.ObtenerTotalVentas(liClientID, liTarjetaID, ValidacionesSistema.DAOObtenerFechaSistema());
            if (ldecTotalVentas >= loConfiguraciones.Monto_Maximo)
            {
                LastErrorMessage = "Se ha excedido el monto máximo permitido por tarjeta, por favor intente más tarde.";
            }
            else
            {
                liTotalRecargas = loValidaciones.ObtenerNumeroRecargas(liClientID, liTarjetaID, ValidacionesSistema.DAOObtenerFechaSistema());
                if (liTotalRecargas >= loConfiguraciones.Maximo_Veces_Recarga)
                {
                    LastErrorMessage = "Se ha excedido el numero de recargas máximo permitido por tarjeta, por favor intente más tarde.";
                }
                else 
                {
                    //Crear nip si el cliente es nuevo
                    if (lbNewClient)
                    {
                        System.Drawing.FontFamily family = new System.Drawing.FontFamily("Tahoma");
                        loCaptchaHandler = new CaptchaImage(150, 50, family); //Para usar loCaptchaHandler es necesario pasarle la tipografía, de otra forma no se podrá ocupar el objeto para crear un NIP
                        lsNIP = loCaptchaHandler.CreateRandomText(7);

                        //Actualizar nip al cliente
                        UpdateClientNIP(liClientID, lsNIP);

                        //Enviar correo
                        lbMailSent = SendNIPMailTo(loCreditCard.CorreoElectronico, lsNIP);
                    }
                    else
                        lbMailSent = true;
                }
            }
            return (lbMailSent);
        }

        private void UpdateClientNIP(int liClientID, string lsNIP)
        {
            ClienteModel loModel = new ClienteModel();
            DACliente ldaDataAccess = new DACliente(msConnectionString);
            DAGenerico ldaGenerico = new DAGenerico(msConnectionString);
            string lsOldNIP = string.Empty;

            loModel = ldaDataAccess.DAConsultEntity("CLIENTE_ID", Convert.ToString(liClientID));
            lsOldNIP = loModel.NIP_Actual;
            loModel.NIP_Actual = lsNIP;
            ldaGenerico.DAOUpdateGeneric("CAT_CLIENTES", "CLIENTE_ID", Convert.ToString(loModel.Cliente_ID), "NIP_ACTUAL", lsNIP, "Bitacora_General", lsOldNIP, 1, ValidacionesSistema.DAOObtenerFechaSistema(), 1);
            
        }

        private int InsertNewCreditCard(string lsNumeroTarjetaCredito)
        {
            TarjetaModel loTarjeta = new TarjetaModel();
            DATarjeta loDATarjeta = new DATarjeta(msConnectionString);
            string lsNumeroTarjetaEncriptada = string.Empty;

            lsNumeroTarjetaEncriptada = COTELConsumer.EncriptarExpresion(lsNumeroTarjetaCredito, DBKEY);
            loTarjeta.Numero = lsNumeroTarjetaEncriptada;
            loTarjeta.Fecha_Ultimo_Uso = ValidacionesSistema.DAOObtenerFechaSistema();
            loTarjeta.Activo = true;
            loTarjeta.Orden = 1;
            loTarjeta.Usuario_ID = 1;
            loTarjeta.Visible = true;
            loDATarjeta.DAInsertarModel(loTarjeta);
            return (loTarjeta.Tarjeta_ID);
        }

        private int InsertNewClient(string lsNombreTarjetahabiente, string lsCorreoElectronico)
        {
            ClienteModel loCliente = new ClienteModel();
            DACliente loDACliente = new DACliente(msConnectionString);
            //string lsNumeroTarjetaEncriptada = string.Empty;

            //lsNumeroTarjetaEncriptada = COTELConsumer.EncriptarExpresion(lsNumeroTarjetaCredito, DBKEY);
            loCliente.Nombre = lsNombreTarjetahabiente;
            loCliente.EMail = lsCorreoElectronico;
            loCliente.NIP_Actual = string.Empty;

            loCliente.Activo = true;
            loCliente.Orden = 1;
            loCliente.Usuario_ID = 1;
            loCliente.Visible = true;
            loDACliente.DAInsertarModel(loCliente);
            return (loCliente.Cliente_ID);
        }

        public void SendRecargaInfoTo(RecargaTelcelModel loRecargaTelcelInfo, CreditCardModel loCreditCardInfo, RecargaInfoModel loRecargaInfo, string lsFolioConecta)
        {
            SendRecargaMailTo(Convert.ToString(loCreditCardInfo.CorreoElectronico), Convert.ToDecimal(loRecargaInfo.MontoRecarga), Convert.ToString(loRecargaInfo.NumeroCelular), loRecargaTelcelInfo.TextoRespuesta, Convert.ToString(loRecargaTelcelInfo.FolioCotel), Convert.ToString(loRecargaTelcelInfo.FolioTelcel), lsFolioConecta);
        }
    }
}