﻿using COTEL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using WebSitesClassLibrary.DataAccess;

namespace COTEL.DataAccess
{
    public class DAClienteCustom01 : DACliente 
    {
        public static string msConnectionString = WebConfigurationManager.ConnectionStrings["SitiosConn"].ConnectionString;
        public DAClienteCustom01(string lsConnectionString): base(lsConnectionString)
        { 
           
        }

        public ClienteModel DAConsultCliente(string lsTarjetaHabiente, string lsCorreoElectronico)
        {
            //decimal ldecTotalVentas = 0;
            DataTable dt = new DataTable();
            ClienteModel loModel = new ClienteModel();
            SqlConnection loSQLConnection;
            SqlCommand loSQLCommand;
            SqlParameter loParameter;

            loSQLConnection = new SqlConnection(msConnectionString);
            loSQLCommand = new SqlCommand("SP_ObtenerCliente", loSQLConnection);

            loParameter = new SqlParameter("Nombre_Cliente", lsTarjetaHabiente);
            loSQLCommand.Parameters.Add(loParameter);

            loParameter = new SqlParameter("Correo_Electronico", lsCorreoElectronico);
            loSQLCommand.Parameters.Add(loParameter);

            loSQLCommand.CommandType = CommandType.StoredProcedure;

            try
            {
                loSQLConnection.Open();

                SqlDataReader dr = loSQLCommand.ExecuteReader();
                dt.Load(dr);

                loSQLConnection.Close();
            }
            catch (SqlException sqlEx)
            {
                throw new Exception(sqlEx.Message);
                //errors = sqlEx.Message;
                //return false;
            }

            if (dt.Rows.Count > 0)
                //ldecTotalVentas = Convert.ToDecimal(dt.Rows[0]["Total_Ventas"].ToString());
                loModel = DataTableToModel(dt);
            

            return (loModel);
        }

    }
}