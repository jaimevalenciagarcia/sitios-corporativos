﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

using COTEL.Models;
using WebSitesClassLibrary.Models;
using WebSitesClassLibrary.DataAccess;

namespace COTEL.DataAccess
{
    public class DACompañia
    {
        #region "Properties"
        public string ConnectionString { get; set; }
        public int LastErrorMessageID { get; set; }
        public string LastErrorMessage { get; set; }
        #endregion

        #region "Constantes"
        private const string NOMBRE_TABLA = "Cat_Compañias";
        private const string NOMBRE_TABLA_BITACORA = "Bitacora_Catalogos";
        private const string NOMBRE_CAMPO_ID = "Compañia_ID";
        #endregion

        #region "Constructor"
        public DACompañia(string lsConnectionString)
        {
            LastErrorMessageID = 0;
            LastErrorMessage = string.Empty;
            ConnectionString = lsConnectionString;
        }
        #endregion

        #region "Private Class Functions"
        /// <summary>
        /// Añade los parámetros de un procedimiento almacenado a una lista.
        /// </summary>
        /// <param name="llstParamList">Lista de tipo SQLParameter pasada por referencia</param>
        /// <param name="lsParamName">Nombre del parámetro que ocupa el procedimiento almacenado</param>
        /// <param name="loValue">Valor que va a asignarse al parámentro</param>
        /// <param name="loDataType">Tipo de dato del parámetro en el procedimiento almacenado</param>
        /// <param name="lbItsAnOutputParam">Opcional, True si el parámetro va retornar un valor del procedimiento almacenado, false en caso contrario</param>
        /// <param name="liSize">Opcional, el tamaño del parametro (se usa para parámetros de tipo caracter (char, nchar, varchar, nvarchar, etc)</param>
        private void AddParameterToList(ref List<SqlParameter> llstParamList, string lsParamName, object loValue, SqlDbType loDataType, bool lbItsAnOutputParam = false, int liSize = -1)
        {
            SqlParameter lSQLParam;
            if (liSize != -1)
                lSQLParam = new SqlParameter(lsParamName, loDataType, liSize);
            else
                lSQLParam = new SqlParameter(lsParamName, loDataType);
            lSQLParam.Value = loValue;
            if (lbItsAnOutputParam == true)
                lSQLParam.Direction = ParameterDirection.Output;
            llstParamList.Add(lSQLParam);
        }

        /// <summary>
        /// Convierte un objeto DataTable a Modelo
        /// </summary>
        /// <param name="loTable">El objeto DataTable a convertir</param>
        /// <returns>El primer registro del objeto DataTable convertido a modelo</returns>
        private CompañiaModel DataTableToModel(DataTable loTable)
        {
            CompañiaModel loModel = new CompañiaModel();

            loModel.Compañia_ID = Convert.ToInt32(loTable.Rows[0]["Compañia_ID"]);
            loModel.RFC = Convert.ToString(loTable.Rows[0]["RFC"]);
            loModel.Nombre_Comercial = Convert.ToString(loTable.Rows[0]["Nombre_Comercial"]);
            loModel.Razon_Social = Convert.ToString(loTable.Rows[0]["Razon_Social"]);
            if (loTable.Rows[0]["Direccion"] != System.DBNull.Value)
                loModel.Direccion = Convert.ToString(loTable.Rows[0]["Direccion"]);
            if (loTable.Rows[0]["Telefono"] != System.DBNull.Value)
                loModel.Telefono = Convert.ToString(loTable.Rows[0]["Telefono"]);
            if (loTable.Rows[0]["Email"] != System.DBNull.Value)
                loModel.Email = Convert.ToString(loTable.Rows[0]["Email"]);
            if (loTable.Rows[0]["Sitio_Web"] != System.DBNull.Value)
                loModel.Sitio_Web = Convert.ToString(loTable.Rows[0]["Sitio_Web"]);
            loModel.Tema_ID = Convert.ToInt32(loTable.Rows[0]["Tema_ID"]);
            loModel.Configuracion_ID = Convert.ToInt32(loTable.Rows[0]["Configuracion_ID"]);
            loModel.Activo = Convert.ToBoolean(loTable.Rows[0]["Activo"]);
            loModel.Orden = Convert.ToInt32(loTable.Rows[0]["Orden"]);
            loModel.Fecha_Creacion = Convert.ToDateTime(loTable.Rows[0]["Fecha_Creacion"]);
            loModel.Fecha_Actualizacion = Convert.ToDateTime(loTable.Rows[0]["Fecha_Actualizacion"]);
            loModel.Usuario_ID = Convert.ToInt32(loTable.Rows[0]["Usuario_ID"]);
            loModel.Visible = Convert.ToBoolean(loTable.Rows[0]["Visible"]);

            return (loModel);
        }

        /// <summary>
        /// Converte todo un DataTable a una lista de Modelos
        /// </summary>
        /// <param name="loDataTable">El objeto DataTable a convertir</param>
        /// <returns>Un objeto de tipo lista de modelos con todos los registros contenidos en el objeto DataTable</returns>
        private List<CompañiaModel> DataTableToList(DataTable loDataTable)
        {
            List<CompañiaModel> llstModels = new List<CompañiaModel>();
            CompañiaModel loModel = new CompañiaModel();
            try
            {
                foreach (DataRow ldRow in loDataTable.Rows)
                {
                    loModel = new CompañiaModel();
                    loModel.Compañia_ID = Convert.ToInt32(ldRow["Compañia_ID"]);
                    loModel.RFC = Convert.ToString(ldRow["RFC"]);
                    loModel.Nombre_Comercial = Convert.ToString(ldRow["Nombre_Comercial"]);
                    loModel.Razon_Social = Convert.ToString(ldRow["Razon_Social"]);
                    if (ldRow["Direccion"] != System.DBNull.Value)
                        loModel.Direccion = Convert.ToString(ldRow["Direccion"]);
                    if (ldRow["Telefono"] != System.DBNull.Value)
                        loModel.Telefono = Convert.ToString(ldRow["Telefono"]);
                    if (ldRow["Email"] != System.DBNull.Value)
                        loModel.Email = Convert.ToString(ldRow["Email"]);
                    if (ldRow["Sitio_Web"] != System.DBNull.Value)
                        loModel.Sitio_Web = Convert.ToString(ldRow["Sitio_Web"]);
                    loModel.Tema_ID = Convert.ToInt32(ldRow["Tema_ID"]);
                    loModel.Configuracion_ID = Convert.ToInt32(ldRow["Configuracion_ID"]);
                    loModel.Activo = Convert.ToBoolean(ldRow["Activo"]);
                    loModel.Orden = Convert.ToInt32(ldRow["Orden"]);
                    loModel.Fecha_Creacion = Convert.ToDateTime(ldRow["Fecha_Creacion"]);
                    loModel.Fecha_Actualizacion = Convert.ToDateTime(ldRow["Fecha_Actualizacion"]);
                    loModel.Usuario_ID = Convert.ToInt32(ldRow["Usuario_ID"]);
                    loModel.Visible = Convert.ToBoolean(ldRow["Visible"]);
                    llstModels.Add(loModel);
                }
            }
            catch (Exception ex)
            {
                LastErrorMessage = ex.Message.ToString();
                throw new Exception(LastErrorMessage);
            }
            return (llstModels);
        }
        #endregion

        #region "Public Class Functions and subs"
        /// <summary>
        /// Inserta un registro en la tabla de Cat_Compañias
        /// </summary>
        /// <param name="loModel">Objeto que contiene los valores a insertar</param>
        /// <returns>True en el caso de que la operación haya fallado, false en caso contrario (ver el valor de la propiedad LastErrorMessage para más información) </returns>
        public int DAInsertarModel(CompañiaModel loModel)
        {
            TablaModel loTableInfo = new TablaModel();
            DATabla lDATabla = new DATabla(ConnectionString);
            DAGenerico daGenerico = new DAGenerico(ConnectionString);
            DataTable dtInsertedRegInfo = new DataTable();
            SqlConnection loSQLConnection;
            SqlCommand loSQLCommand;
            List<SqlParameter> llstParameters = new List<SqlParameter>();

            int liRegIDInserted = -1;
            int liAffectedRows;
            int liOrden = 1;
            int liUsuarioID = -1;
            DateTime dtFechaInsercion;

            AddParameterToList(ref llstParameters, loModel.ParamNames.Compañia_ID, loModel.Compañia_ID, SqlDbType.Int, true);
            AddParameterToList(ref llstParameters, loModel.ParamNames.RFC, loModel.RFC, SqlDbType.NVarChar);
            AddParameterToList(ref llstParameters, loModel.ParamNames.Nombre_Comercial, loModel.Nombre_Comercial, SqlDbType.NVarChar);
            AddParameterToList(ref llstParameters, loModel.ParamNames.Razon_Social, loModel.Razon_Social, SqlDbType.NVarChar);
            if (loModel.Direccion != string.Empty)
                AddParameterToList(ref llstParameters, loModel.ParamNames.Direccion, loModel.Direccion, SqlDbType.NVarChar);
            if (loModel.Telefono != string.Empty)
                AddParameterToList(ref llstParameters, loModel.ParamNames.Telefono, loModel.Telefono, SqlDbType.NVarChar);
            if (loModel.Email != string.Empty)
                AddParameterToList(ref llstParameters, loModel.ParamNames.Email, loModel.Email, SqlDbType.NVarChar);
            if (loModel.Sitio_Web != string.Empty)
                AddParameterToList(ref llstParameters, loModel.ParamNames.Sitio_Web, loModel.Sitio_Web, SqlDbType.NVarChar);
            AddParameterToList(ref llstParameters, loModel.ParamNames.Tema_ID, loModel.Tema_ID, SqlDbType.Int);
            AddParameterToList(ref llstParameters, loModel.ParamNames.Configuracion_ID, loModel.Configuracion_ID, SqlDbType.Int);
            AddParameterToList(ref llstParameters, loModel.ParamNames.Activo, loModel.Activo, SqlDbType.Bit);
            AddParameterToList(ref llstParameters, loModel.ParamNames.Orden, loModel.Orden, SqlDbType.Int);
            AddParameterToList(ref llstParameters, loModel.ParamNames.Fecha_Creacion, loModel.Fecha_Creacion, SqlDbType.SmallDateTime, true);
            AddParameterToList(ref llstParameters, loModel.ParamNames.Fecha_Actualizacion, loModel.Fecha_Actualizacion, SqlDbType.SmallDateTime, true);
            AddParameterToList(ref llstParameters, loModel.ParamNames.Usuario_ID, loModel.Usuario_ID, SqlDbType.Int);
            AddParameterToList(ref llstParameters, loModel.ParamNames.Visible, loModel.Visible, SqlDbType.Bit);

            loSQLConnection = new SqlConnection(ConnectionString);
            loSQLCommand = new SqlCommand(loModel.StoredProcNames.Insert, loSQLConnection);
            loSQLCommand.CommandType = CommandType.StoredProcedure;
            loSQLCommand.Parameters.AddRange(llstParameters.ToArray());

            try
            {
                loSQLConnection.Open();
                liAffectedRows = loSQLCommand.ExecuteNonQuery();
                if (liAffectedRows > 0)
                {
                    loModel.Compañia_ID = Convert.ToInt32(loSQLCommand.Parameters[loModel.ParamNames.Compañia_ID].Value);
                    loModel.Fecha_Actualizacion = Convert.ToDateTime(loSQLCommand.Parameters[loModel.ParamNames.Fecha_Actualizacion].Value);
                    loModel.Fecha_Creacion = Convert.ToDateTime(loSQLCommand.Parameters[loModel.ParamNames.Fecha_Creacion].Value);
                    liRegIDInserted = loModel.Compañia_ID = Convert.ToInt32(loSQLCommand.Parameters[loModel.ParamNames.Compañia_ID].Value);

                    // -------- Bitácora --------
                    dtInsertedRegInfo = DAConsultTable(liRegIDInserted);
                    loTableInfo = lDATabla.DAConsultEntity("Nombre", NOMBRE_TABLA);
                    dtFechaInsercion = daGenerico.DAOObtenerFechaSistema();
                    liUsuarioID = DAGenerico.IDUsuarioActual();
                    if (dtInsertedRegInfo.Rows.Count == 1 && dtInsertedRegInfo.Columns.Count > 0)
                    {
                        for (int liColumnCount = 0; liColumnCount <= dtInsertedRegInfo.Columns.Count - 1; liColumnCount++)
                        {
                            daGenerico.DAOBitacoraGeneric(NOMBRE_TABLA_BITACORA, loTableInfo.Tabla_ID, liRegIDInserted, dtInsertedRegInfo.Columns[liColumnCount].ColumnName, null, dtInsertedRegInfo.Rows[0][liColumnCount], liUsuarioID, dtFechaInsercion, liOrden++);
                        }
                    }
                    // -------- Bitácora --------
                }
                else
                {
                    LastErrorMessage = "Error en SP al insertar registro, no se insertó ningún registro";
                    throw new Exception(LastErrorMessage);
                }
            }
            catch (Exception ex)
            {
                LastErrorMessage = ex.Message.ToString();
                throw new Exception(LastErrorMessage);
            }
            finally
            {
                loSQLConnection.Close();
            }
            return (liRegIDInserted);
        }

        /// <summary>
        /// Obtiene una lista de tipo IEnumerable de modelos
        /// </summary>
        /// <param name="liCampoID">Opcional, campo identificador único del registro, -1 para obtener todos los registros de la tabla</param>
        /// <returns>Lista de tipo IEnumerable de modelos</returns>
        public IEnumerable<CompañiaModel> DAConsultIEnumerable(int liCampoID = -1)
        {
            DAGenerico loDAGenerico = new DAGenerico(ConnectionString);
            IEnumerable<CompañiaModel> lIEModelInfo = new List<CompañiaModel>();
            DataTable loDataTable;

            if (liCampoID != -1)
                loDataTable = loDAGenerico.DAOSelectGeneric(NOMBRE_TABLA, DAGenerico.OperadoresSelect.CampoIgualAValor, NOMBRE_CAMPO_ID, Convert.ToString(liCampoID));
            else
                loDataTable = loDAGenerico.DAOSelectGeneric(NOMBRE_TABLA, DAGenerico.OperadoresSelect.Todos);
            lIEModelInfo = DataTableToList(loDataTable);

            return (lIEModelInfo);
        }

        /// <summary>
        /// Obtiene una lista de tipo IEnumerable de modelos
        /// </summary>
        /// <param name="lsNombreCampo">Nombre del campo a consultar</param>
        /// <param name="lsValor">Valor del campo</param>
        /// <returns>Lista de tipo IEnumerable de modelos</returns>
        public IEnumerable<CompañiaModel> DAConsultIEnumerable(string lsNombreCampo, string lsValor)
        {
            DAGenerico loDAGenerico = new DAGenerico(ConnectionString);
            IEnumerable<CompañiaModel> lIEModelInfo = new List<CompañiaModel>();
            DataTable loDataTable;

            loDataTable = loDAGenerico.DAOSelectGeneric(NOMBRE_TABLA, DAGenerico.OperadoresSelect.CampoIgualAValor, lsNombreCampo, lsValor);
            lIEModelInfo = DataTableToList(loDataTable);

            return (lIEModelInfo);
        }

        /// <summary>
        /// Obtiene una lista de modelos
        /// </summary>
        /// <param name="liCampoID">Opcional, campo identificador único del registro, -1 para obtener todos los registros de la tabla</param>
        /// <returns>Lista de tipo modelos</returns>
        public List<CompañiaModel> DAConsultList(int liCampoID = -1)
        {
            DAGenerico loDAGenerico = new DAGenerico(ConnectionString);
            List<CompañiaModel> llstModelInfo = new List<CompañiaModel>();
            DataTable loDataTable;

            if (liCampoID != -1)
                loDataTable = loDAGenerico.DAOSelectGeneric(NOMBRE_TABLA, DAGenerico.OperadoresSelect.CampoIgualAValor, NOMBRE_CAMPO_ID, Convert.ToString(liCampoID));
            else
                loDataTable = loDAGenerico.DAOSelectGeneric(NOMBRE_TABLA, DAGenerico.OperadoresSelect.Todos);
            llstModelInfo = DataTableToList(loDataTable);

            return (llstModelInfo);
        }

        /// <summary>
        /// Obtiene una lista de modelos
        /// </summary>
        /// <param name="lsNombre_Campo">Nombre del campo a consultar</param>
        /// <param name="lsValor">Valor del campo</param>
        /// <returns>Lista de tipo modelos</returns>
        public List<CompañiaModel> DAConsultList(string lsNombre_Campo, string lsValor)
        {
            DAGenerico loDAGenerico = new DAGenerico(ConnectionString);
            List<CompañiaModel> llstModelInfo = new List<CompañiaModel>();
            DataTable loDataTable;

            loDataTable = loDAGenerico.DAOSelectGeneric(NOMBRE_TABLA, DAGenerico.OperadoresSelect.CampoIgualAValor, lsNombre_Campo, lsValor);
            llstModelInfo = DataTableToList(loDataTable);

            return (llstModelInfo);
        }

        /// <summary>
        /// Obtiene un objeto DataTable
        /// </summary>
        /// <param name="liCampoID">Opcional, campo identificador único del registro, -1 para obtener todos los registros de la tabla</param>
        /// <returns>Un objeto de tipo DataTable</returns>
        public DataTable DAConsultTable(int liCampoID = -1)
        {
            DAGenerico loDAGenerico = new DAGenerico(ConnectionString);
            DataTable loTable = new DataTable();

            if (liCampoID != -1)
                loTable = loDAGenerico.DAOSelectGeneric(NOMBRE_TABLA, DAGenerico.OperadoresSelect.CampoIgualAValor, NOMBRE_CAMPO_ID, Convert.ToString(liCampoID));
            else
                loTable = loDAGenerico.DAOSelectGeneric(NOMBRE_TABLA, DAGenerico.OperadoresSelect.Todos);

            return (loTable);
        }

        /// <summary>
        /// Obtiene un objeto de tipo Model
        /// </summary>
        /// <param name="liCampoID">Opcional, campo identificador único del registro, -1 para obtener todos los registros de la tabla</param>
        /// <returns>Un objeto de tipo Model</returns>
        public CompañiaModel DAConsultEntity(int liCampoID = -1)
        {
            DAGenerico loDAGenerico = new DAGenerico(ConnectionString);
            CompañiaModel loModel = new CompañiaModel();
            DataTable loDataTable;

            if (liCampoID != -1)
                loDataTable = loDAGenerico.DAOSelectGeneric(NOMBRE_TABLA, DAGenerico.OperadoresSelect.CampoIgualAValor, NOMBRE_CAMPO_ID, Convert.ToString(liCampoID));
            else
                loDataTable = loDAGenerico.DAOSelectGeneric(NOMBRE_TABLA, DAGenerico.OperadoresSelect.Todos);
            loModel = DataTableToModel(loDataTable);

            return loModel;
        }

        /// <summary>
        /// Obtiene un objeto de tipo Model
        /// </summary>
        /// <param name="lsNombreCampo">Nombre del campo a consultar</param>
        /// <param name="lsValor">Valor del campo</param>
        /// <returns>Un objeto de tipo Model</returns>
        public CompañiaModel DAConsultEntity(string lsNombreCampo, string lsValor)
        {
            DAGenerico loDAGenerico = new DAGenerico(ConnectionString);
            CompañiaModel loModel = new CompañiaModel();
            DataTable loDataTable;

            loDataTable = loDAGenerico.DAOSelectGeneric(NOMBRE_TABLA, DAGenerico.OperadoresSelect.CampoIgualAValor, lsNombreCampo, lsValor);
            loModel = DataTableToModel(loDataTable);

            return loModel;
        }
        #endregion

        #region Custom
        public CompañiaModel DAOConsultCompany(string sRFC)
        {
            CompañiaModel mCompany = new CompañiaModel();

            DataTable dt = new DataTable();

            SqlConnection con = new SqlConnection(ConnectionString);
            SqlCommand com = new SqlCommand("sp_ConsultaCompañia", con);
            com.CommandType = System.Data.CommandType.StoredProcedure;

            SqlParameter spRFC = new SqlParameter("RFC", sRFC);
            com.Parameters.Add(spRFC);

            try
            {
                con.Open();

                SqlDataReader dr = com.ExecuteReader();
                dt.Load(dr);

                con.Close();

                mCompany.Compañia_ID = Convert.ToInt32(dt.Rows[0]["Compañia_ID"].ToString());
                mCompany.RFC = dt.Rows[0]["RFC"].ToString();
                mCompany.Nombre_Comercial = dt.Rows[0]["Nombre_Comercial"].ToString();
                mCompany.Razon_Social = dt.Rows[0]["Razon_Social"].ToString();
                mCompany.Direccion = dt.Rows[0]["Direccion"].ToString();
                mCompany.Telefono = dt.Rows[0]["Telefono"].ToString();
                mCompany.Email = dt.Rows[0]["Email"].ToString();
                mCompany.Sitio_Web = dt.Rows[0]["Sitio_Web"].ToString();
                mCompany.Tema_ID = Convert.ToInt32(dt.Rows[0]["Tema_ID"].ToString());
                mCompany.Configuracion_ID = Convert.ToInt32(dt.Rows[0]["Configuracion_ID"].ToString());
                mCompany.Activo = Convert.ToBoolean(dt.Rows[0]["Activo"].ToString());
                mCompany.Orden = Convert.ToInt32(dt.Rows[0]["Orden"].ToString());
                mCompany.Fecha_Creacion = Convert.ToDateTime(dt.Rows[0]["Fecha_Creacion"].ToString());
                mCompany.Fecha_Actualizacion = Convert.ToDateTime(dt.Rows[0]["Fecha_Actualizacion"].ToString());
                mCompany.Usuario_ID = Convert.ToInt32(dt.Rows[0]["Usuario_ID"].ToString());
                mCompany.Visible = Convert.ToBoolean(dt.Rows[0]["Visible"].ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return mCompany;
        }

        #endregion
    }
}