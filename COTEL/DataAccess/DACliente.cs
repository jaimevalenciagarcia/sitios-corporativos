﻿using COTEL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WebSitesClassLibrary.DataAccess;
using WebSitesClassLibrary.Models;

namespace COTEL.DataAccess
{
    public class DACliente
    {
        #region "Properties"
        public string ConnectionString { get; set; }
        public int LastErrorMessageID { get; set; }
        public string LastErrorMessage { get; set; }
        #endregion

        #region "Constantes"
        private const string NOMBRE_TABLA = "Cat_Clientes";
        private const string NOMBRE_TABLA_BITACORA = "Bitacora_Catalogos";
        private const string NOMBRE_CAMPO_ID = "Cliente_ID";
        #endregion

        #region "Constructor"
        public DACliente(string lsConnectionString)
        {
            LastErrorMessageID = 0;
            LastErrorMessage = string.Empty;
            ConnectionString = lsConnectionString;
        }
        #endregion

        #region "Private Class Functions"
        /// <summary>
        /// Añade los parámetros de un procedimiento almacenado a una lista.
        /// </summary>
        /// <param name="llstParamList">Lista de tipo SQLParameter pasada por referencia</param>
        /// <param name="lsParamName">Nombre del parámetro que ocupa el procedimiento almacenado</param>
        /// <param name="loValue">Valor que va a asignarse al parámentro</param>
        /// <param name="loDataType">Tipo de dato del parámetro en el procedimiento almacenado</param>
        /// <param name="lbItsAnOutputParam">Opcional, True si el parámetro va retornar un valor del procedimiento almacenado, false en caso contrario</param>
        /// <param name="liSize">Opcional, el tamaño del parametro (se usa para parámetros de tipo caracter (char, nchar, varchar, nvarchar, etc)</param>
        private void AddParameterToList(ref List<SqlParameter> llstParamList, string lsParamName, object loValue, SqlDbType loDataType, bool lbItsAnOutputParam = false, int liSize = -1)
        {
            SqlParameter lSQLParam;
            if (liSize != -1)
                lSQLParam = new SqlParameter(lsParamName, loDataType, liSize);
            else
                lSQLParam = new SqlParameter(lsParamName, loDataType);
            lSQLParam.Value = loValue;
            if (lbItsAnOutputParam == true)
                lSQLParam.Direction = ParameterDirection.Output;
            llstParamList.Add(lSQLParam);
        }

        /// <summary>
        /// Convierte un objeto DataTable a Modelo
        /// </summary>
        /// <param name="loTable">El objeto DataTable a convertir</param>
        /// <returns>El primer registro del objeto DataTable convertido a modelo</returns>
        public ClienteModel DataTableToModel(DataTable loTable)
        {
            ClienteModel loModel = new ClienteModel();

            if (loTable.Rows.Count > 0)
            {
                loModel.Cliente_ID = Convert.ToInt32(loTable.Rows[0]["Cliente_ID"]);
                loModel.Nombre = Convert.ToString(loTable.Rows[0]["Nombre"]);
                loModel.EMail = Convert.ToString(loTable.Rows[0]["EMail"]);
                loModel.NIP_Actual = Convert.ToString(loTable.Rows[0]["NIP_Actual"]);
                loModel.Activo = Convert.ToBoolean(loTable.Rows[0]["Activo"]);
                loModel.Orden = Convert.ToInt32(loTable.Rows[0]["Orden"]);
                loModel.Fecha_Creacion = Convert.ToDateTime(loTable.Rows[0]["Fecha_Creacion"]);
                loModel.Fecha_Actualizacion = Convert.ToDateTime(loTable.Rows[0]["Fecha_Actualizacion"]);
                loModel.Usuario_ID = Convert.ToInt32(loTable.Rows[0]["Usuario_ID"]);
                loModel.Visible = Convert.ToBoolean(loTable.Rows[0]["Visible"]);
            }
            return (loModel);
        }

        /// <summary>
        /// Converte todo un DataTable a una lista de Modelos
        /// </summary>
        /// <param name="loDataTable">El objeto DataTable a convertir</param>
        /// <returns>Un objeto de tipo lista de modelos con todos los registros contenidos en el objeto DataTable</returns>
        private List<ClienteModel> DataTableToList(DataTable loDataTable)
        {
            List<ClienteModel> llstModels = new List<ClienteModel>();
            ClienteModel loModel = new ClienteModel();
            try
            {
                foreach (DataRow ldRow in loDataTable.Rows)
                {
                    loModel = new ClienteModel();
                    loModel.Cliente_ID = Convert.ToInt32(ldRow["Cliente_ID"]);
                    loModel.Nombre = Convert.ToString(ldRow["Nombre"]);
                    loModel.EMail = Convert.ToString(ldRow["EMail"]);
                    loModel.NIP_Actual = Convert.ToString(ldRow["NIP_Actual"]);
                    loModel.Activo = Convert.ToBoolean(ldRow["Activo"]);
                    loModel.Orden = Convert.ToInt32(ldRow["Orden"]);
                    loModel.Fecha_Creacion = Convert.ToDateTime(ldRow["Fecha_Creacion"]);
                    loModel.Fecha_Actualizacion = Convert.ToDateTime(ldRow["Fecha_Actualizacion"]);
                    loModel.Usuario_ID = Convert.ToInt32(ldRow["Usuario_ID"]);
                    loModel.Visible = Convert.ToBoolean(ldRow["Visible"]);
                    llstModels.Add(loModel);
                }
            }
            catch (Exception ex)
            {
                LastErrorMessage = ex.Message.ToString();
                throw new Exception(LastErrorMessage);
            }
            return (llstModels);
        }
        #endregion

        #region "Public Class Functions and subs"
        /// <summary>
        /// Inserta un registro en la tabla de Cat_Clientes
        /// </summary>
        /// <param name="loModel">Objeto que contiene los valores a insertar</param>
        /// <returns>True en el caso de que la operación haya fallado, false en caso contrario (ver el valor de la propiedad LastErrorMessage para más información) </returns>
        public int DAInsertarModel(ClienteModel loModel)
        {
            TablaModel loTableInfo = new TablaModel();
            DATabla lDATabla = new DATabla(ConnectionString);
            DAGenerico daGenerico = new DAGenerico(ConnectionString);
            DataTable dtInsertedRegInfo = new DataTable();
            SqlConnection loSQLConnection;
            SqlCommand loSQLCommand;
            List<SqlParameter> llstParameters = new List<SqlParameter>();

            int liRegIDInserted = -1;
            int liAffectedRows;
            int liOrden = 1;
            int liUsuarioID = -1;
            DateTime dtFechaInsercion;

            AddParameterToList(ref llstParameters, loModel.ParamNames.Cliente_ID, loModel.Cliente_ID, SqlDbType.Int, true);
            AddParameterToList(ref llstParameters, loModel.ParamNames.Nombre, loModel.Nombre, SqlDbType.NVarChar);
            AddParameterToList(ref llstParameters, loModel.ParamNames.EMail, loModel.EMail, SqlDbType.NVarChar);
            AddParameterToList(ref llstParameters, loModel.ParamNames.NIP_Actual, loModel.NIP_Actual, SqlDbType.NVarChar);
            AddParameterToList(ref llstParameters, loModel.ParamNames.Activo, loModel.Activo, SqlDbType.Bit);
            AddParameterToList(ref llstParameters, loModel.ParamNames.Orden, loModel.Orden, SqlDbType.Int);
            AddParameterToList(ref llstParameters, loModel.ParamNames.Fecha_Creacion, loModel.Fecha_Creacion, SqlDbType.DateTime, true);
            AddParameterToList(ref llstParameters, loModel.ParamNames.Fecha_Actualizacion, loModel.Fecha_Actualizacion, SqlDbType.DateTime, true);
            AddParameterToList(ref llstParameters, loModel.ParamNames.Usuario_ID, loModel.Usuario_ID, SqlDbType.Int);
            AddParameterToList(ref llstParameters, loModel.ParamNames.Visible, loModel.Visible, SqlDbType.Bit);

            loSQLConnection = new SqlConnection(ConnectionString);
            loSQLCommand = new SqlCommand(loModel.StoredProcNames.Insert, loSQLConnection);
            loSQLCommand.CommandType = CommandType.StoredProcedure;
            loSQLCommand.Parameters.AddRange(llstParameters.ToArray());

            try
            {
                loSQLConnection.Open();
                liAffectedRows = loSQLCommand.ExecuteNonQuery();
                if (liAffectedRows > 0)
                {
                    loModel.Cliente_ID = Convert.ToInt32(loSQLCommand.Parameters[loModel.ParamNames.Cliente_ID].Value);
                    loModel.Fecha_Actualizacion = Convert.ToDateTime(loSQLCommand.Parameters[loModel.ParamNames.Fecha_Actualizacion].Value);
                    loModel.Fecha_Creacion = Convert.ToDateTime(loSQLCommand.Parameters[loModel.ParamNames.Fecha_Creacion].Value);
                    liRegIDInserted = loModel.Cliente_ID = Convert.ToInt32(loSQLCommand.Parameters[loModel.ParamNames.Cliente_ID].Value);

                    // -------- Bitácora --------
                    dtInsertedRegInfo = DAConsultTable(liRegIDInserted);
                    loTableInfo = lDATabla.DAConsultEntity("Nombre", NOMBRE_TABLA);
                    dtFechaInsercion = daGenerico.DAOObtenerFechaSistema();
                    liUsuarioID = DAGenerico.IDUsuarioActual();
                    if (dtInsertedRegInfo.Rows.Count == 1 && dtInsertedRegInfo.Columns.Count > 0)
                    {
                        for (int liColumnCount = 0; liColumnCount <= dtInsertedRegInfo.Columns.Count - 1; liColumnCount++)
                        {
                            daGenerico.DAOBitacoraGeneric(NOMBRE_TABLA_BITACORA, loTableInfo.Tabla_ID, liRegIDInserted, dtInsertedRegInfo.Columns[liColumnCount].ColumnName, null, dtInsertedRegInfo.Rows[0][liColumnCount], liUsuarioID, dtFechaInsercion, liOrden++);
                        }
                    }
                    // -------- Bitácora --------
                }
                else
                {
                    LastErrorMessage = "Error en SP al insertar registro, no se insertó ningún registro";
                    throw new Exception(LastErrorMessage);
                }
            }
            catch (Exception ex)
            {
                LastErrorMessage = ex.Message.ToString();
                throw new Exception(LastErrorMessage);
            }
            finally
            {
                loSQLConnection.Close();
            }
            return (liRegIDInserted);
        }

        /// <summary>
        /// Obtiene una lista de tipo IEnumerable de modelos
        /// </summary>
        /// <param name="liCampoID">Opcional, campo identificador único del registro, -1 para obtener todos los registros de la tabla</param>
        /// <returns>Lista de tipo IEnumerable de modelos</returns>
        public IEnumerable<ClienteModel> DAConsultIEnumerable(int liCampoID = -1)
        {
            DAGenerico loDAGenerico = new DAGenerico(ConnectionString);
            IEnumerable<ClienteModel> lIEModelInfo = new List<ClienteModel>();
            DataTable loDataTable;

            if (liCampoID != -1)
                loDataTable = loDAGenerico.DAOSelectGeneric(NOMBRE_TABLA, DAGenerico.OperadoresSelect.CampoIgualAValor, NOMBRE_CAMPO_ID, Convert.ToString(liCampoID));
            else
                loDataTable = loDAGenerico.DAOSelectGeneric(NOMBRE_TABLA, DAGenerico.OperadoresSelect.Todos);
            lIEModelInfo = DataTableToList(loDataTable);

            return (lIEModelInfo);
        }

        /// <summary>
        /// Obtiene una lista de tipo IEnumerable de modelos
        /// </summary>
        /// <param name="lsNombreCampo">Nombre del campo a consultar</param>
        /// <param name="lsValor">Valor del campo</param>
        /// <returns>Lista de tipo IEnumerable de modelos</returns>
        public IEnumerable<ClienteModel> DAConsultIEnumerable(string lsNombreCampo, string lsValor)
        {
            DAGenerico loDAGenerico = new DAGenerico(ConnectionString);
            IEnumerable<ClienteModel> lIEModelInfo = new List<ClienteModel>();
            DataTable loDataTable;

            loDataTable = loDAGenerico.DAOSelectGeneric(NOMBRE_TABLA, DAGenerico.OperadoresSelect.CampoIgualAValor, lsNombreCampo, lsValor);
            lIEModelInfo = DataTableToList(loDataTable);

            return (lIEModelInfo);
        }

        /// <summary>
        /// Obtiene una lista de modelos
        /// </summary>
        /// <param name="liCampoID">Opcional, campo identificador único del registro, -1 para obtener todos los registros de la tabla</param>
        /// <returns>Lista de tipo modelos</returns>
        public List<ClienteModel> DAConsultList(int liCampoID = -1)
        {
            DAGenerico loDAGenerico = new DAGenerico(ConnectionString);
            List<ClienteModel> llstModelInfo = new List<ClienteModel>();
            DataTable loDataTable;

            if (liCampoID != -1)
                loDataTable = loDAGenerico.DAOSelectGeneric(NOMBRE_TABLA, DAGenerico.OperadoresSelect.CampoIgualAValor, NOMBRE_CAMPO_ID, Convert.ToString(liCampoID));
            else
                loDataTable = loDAGenerico.DAOSelectGeneric(NOMBRE_TABLA, DAGenerico.OperadoresSelect.Todos);
            llstModelInfo = DataTableToList(loDataTable);

            return (llstModelInfo);
        }

        /// <summary>
        /// Obtiene una lista de modelos
        /// </summary>
        /// <param name="lsNombre_Campo">Nombre del campo a consultar</param>
        /// <param name="lsValor">Valor del campo</param>
        /// <returns>Lista de tipo modelos</returns>
        public List<ClienteModel> DAConsultList(string lsNombre_Campo, string lsValor)
        {
            DAGenerico loDAGenerico = new DAGenerico(ConnectionString);
            List<ClienteModel> llstModelInfo = new List<ClienteModel>();
            DataTable loDataTable;

            loDataTable = loDAGenerico.DAOSelectGeneric(NOMBRE_TABLA, DAGenerico.OperadoresSelect.CampoIgualAValor, lsNombre_Campo, lsValor);
            llstModelInfo = DataTableToList(loDataTable);

            return (llstModelInfo);
        }

        /// <summary>
        /// Obtiene un objeto DataTable
        /// </summary>
        /// <param name="liCampoID">Opcional, campo identificador único del registro, -1 para obtener todos los registros de la tabla</param>
        /// <returns>Un objeto de tipo DataTable</returns>
        public DataTable DAConsultTable(int liCampoID = -1)
        {
            DAGenerico loDAGenerico = new DAGenerico(ConnectionString);
            DataTable loTable = new DataTable();

            if (liCampoID != -1)
                loTable = loDAGenerico.DAOSelectGeneric(NOMBRE_TABLA, DAGenerico.OperadoresSelect.CampoIgualAValor, NOMBRE_CAMPO_ID, Convert.ToString(liCampoID));
            else
                loTable = loDAGenerico.DAOSelectGeneric(NOMBRE_TABLA, DAGenerico.OperadoresSelect.Todos);

            return (loTable);
        }

        /// <summary>
        /// Obtiene un objeto de tipo Model
        /// </summary>
        /// <param name="liCampoID">Opcional, campo identificador único del registro, -1 para obtener todos los registros de la tabla</param>
        /// <returns>Un objeto de tipo Model</returns>
        public ClienteModel DAConsultEntity(int liCampoID = -1)
        {
            DAGenerico loDAGenerico = new DAGenerico(ConnectionString);
            ClienteModel loModel = new ClienteModel();
            DataTable loDataTable;

            if (liCampoID != -1)
                loDataTable = loDAGenerico.DAOSelectGeneric(NOMBRE_TABLA, DAGenerico.OperadoresSelect.CampoIgualAValor, NOMBRE_CAMPO_ID, Convert.ToString(liCampoID));
            else
                loDataTable = loDAGenerico.DAOSelectGeneric(NOMBRE_TABLA, DAGenerico.OperadoresSelect.Todos);
            loModel = DataTableToModel(loDataTable);

            return loModel;
        }

        /// <summary>
        /// Obtiene un objeto de tipo Model
        /// </summary>
        /// <param name="lsNombreCampo">Nombre del campo a consultar</param>
        /// <param name="lsValor">Valor del campo</param>
        /// <returns>Un objeto de tipo Model</returns>
        public ClienteModel DAConsultEntity(string lsNombreCampo, string lsValor)
        {
            DAGenerico loDAGenerico = new DAGenerico(ConnectionString);
            ClienteModel loModel = new ClienteModel();
            DataTable loDataTable;

            loDataTable = loDAGenerico.DAOSelectGeneric(NOMBRE_TABLA, DAGenerico.OperadoresSelect.CampoIgualAValor, lsNombreCampo, lsValor);
            loModel = DataTableToModel(loDataTable);

            return loModel;
        }
        #endregion


    }
}