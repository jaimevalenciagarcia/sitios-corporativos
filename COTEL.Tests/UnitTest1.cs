﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using COTEL.Models;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Xml.Serialization;
using COTEL.Classes;
using System.IO;
using System.Security.Cryptography;
using System.Web;

namespace COTEL.Tests
{
    [TestClass]
    public class UnitTest1
    {

        public const string CURRENT_IP = "201.122.220.18";
        public const string SERVICE_GET_SALDO = "TAEServices2.0.6/resources/saldo";
        public const string SERVICE_ABONAR = "TAEServices2.0.6/resources/abono";
        public const string CURRENT_KEY = "m3x1c02013a2c4e5";

        public const string FIRST_URL_PARAMETER = "tsolicitud=C02G8416DRJM&cn=&rsolicitud=";
        public const string XML_GET_SALDO = "<?xml version='1.0' encoding='UTF-8'?><Solicitud kidentificador='426' strlogin='jpoumian' spassword='B1tware14' strmacadress='MBL9CAL0-C0C8-4H9C-ABG3-3246CH'></Solicitud>";
        

        [TestMethod]
        public void TestPopulatePago()
        {
            ConektaPagoTarjeta loPagoTarjeta = new ConektaPagoTarjeta();
            ConektaDetalles loDetalle = new ConektaDetalles();
            ConektaItem loItem = new ConektaItem();

            loPagoTarjeta.description = "TAE";
            loPagoTarjeta.amount = 10000; //La API de conekta no acepta puntos decimales
            loPagoTarjeta.currency = "MXN";
            loPagoTarjeta.reference_id = "9839-TAE"; //Para control interno de la tienda
            loPagoTarjeta.card = "";
            loPagoTarjeta.details = new List<ConektaDetalles>();

            loItem.name = "Tiempo Aire Electrónico";
            loItem.sku = "TAE20150511";
            loItem.unit_price = 10000;
            loItem.description = "TAE para Telcel";
            loItem.quantity = 1;
            loItem.type = "Telefonía Celular";

            loDetalle.name = "Luis Duran";
            loDetalle.email = "test@test.com";
            loDetalle.phone = "5569878758";

            loDetalle.line_items.Add(loItem);
            loPagoTarjeta.details.Add(loDetalle);

        }

        [TestMethod]
        public void TestCalling()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://api.conekta.io/");
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Clear();
                //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, "relativeAddress");
                //req.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.Timeout = new System.TimeSpan(0, 0, 30);
                //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/vnd.conekta-v0.3.0+json"));
                //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", "a2V5X0NYZFFyR0MyeFB0THp4M1ZxZnljenJR");
                
                //HttpClient c = new HttpClient();
                //c.BaseAddress = new Uri("http://example.com/"); 
                //c.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json")); 
                //HttpRequestMessage req = new HttpRequestMessage(HttpMethod.Post, "relativeAddress"); 
                //req.Content = new StringContent("{\"name\":\"John Doe\",\"age\":33}", Encoding.UTF8, "application/json");
                
                Assert.IsTrue(client.DefaultRequestHeaders.ToString().Contains("Content-Type"));
            }
        }

        [TestMethod]
        public void DeserialXML()
        {
            var serializer = new XmlSerializer(typeof(responseBean));
            responseBean result;
            string lsXMLSaldoInfo;
            string lsInfoFromService;

            lsInfoFromService = "<?xml version=" + (char)34 + "1.0" + (char)34 + " encoding=" + (char)34 + "UTF-8" + (char)34 + " standalone=" + (char)34 + "yes" + (char)34 + "?><responseBean><c_response>1</c_response><s_response>J/U8UqB4gDnc3YyJPaGsWMaglsUdREbfOdVg6gDsF2Gi75vvNNPo/ujnJpntftGRdkky6DKp87Qk&#xD;4KLlIpz+qgYoEgpsPqH4/lyHK/fkpS+oOsL8KYjK99cu3IXbmBgvnhj117G5lPCkbe5h/yaPrw==</s_response></responseBean>";

            using (TextReader reader = new StringReader(lsInfoFromService))
            {
                result = (responseBean)serializer.Deserialize(reader);
                lsXMLSaldoInfo = result.s_response;
                lsXMLSaldoInfo = DesencriptarExpresion(lsXMLSaldoInfo);
                lsXMLSaldoInfo = lsXMLSaldoInfo.Replace("\r\n", "").Replace("", "").Replace("    ", "");
                var xmlAbonoSerializer = new XmlSerializer(typeof(abonoMobileBean));
                abonoMobileBean loResponseAbono;
                using (TextReader loAbonoReader = new StringReader(lsXMLSaldoInfo))
                {
                    loResponseAbono = (abonoMobileBean)serializer.Deserialize(loAbonoReader);
                    //Obtener información adicional de la recarga.
                }
            }
        }

        public static string EncriptarExpresion(string lsExpresion)
        {
            string lsEncryptedExp = string.Empty;
            byte[] encryptedPassword;

            // Create a new instance of the RijndaelManaged
            // class.  This generates a new key and initialization
            // vector (IV).
            using (var algorithm = new AesManaged())
            {
                algorithm.KeySize = 256; //256;
                algorithm.BlockSize = 128;

                // Encrypt the string to an array of bytes.
                //------------------------------------------------------
                string lsExpToEncrypt = lsExpresion; // "<?xml version='1.0' encoding='UTF-8'?><Solicitud kidentificador='426' strlogin='jpoumian' spassword='B1tware14' strmacadress='MBL9CAL0-C0C8-4H9C-ABG3-3246CH'></Solicitud>";
                Encoding enc = new UTF8Encoding(true, true);
                byte[] strKeyValue = null;
                strKeyValue = enc.GetBytes(CURRENT_KEY);
                //-------------------------------------------------------

                encryptedPassword = Cryptology.EncryptStringToBytes(lsExpToEncrypt, strKeyValue, algorithm.IV);
                string lsValue = Convert.ToBase64String(encryptedPassword);
                string lsEncodedValue = HttpUtility.UrlEncode(lsValue, Encoding.UTF8);
                lsEncryptedExp = lsEncodedValue;
            }
            return (lsEncryptedExp);
        }

        public static string DesencriptarExpresion(string lsExpresion)
        {
            // Create a new instance of the RijndaelManaged
            // class.  This generates a new key and initialization
            // vector (IV).
            using (var algorithm = new AesManaged())//  Rijndael())//  RijndaelManaged())
            {
                algorithm.KeySize = 256; //256;
                algorithm.BlockSize = 128;

                //// Encrypt the string to an array of bytes.
                ////------------------------------------------------------
                Encoding enc = new UTF8Encoding(true, true);
                byte[] strKeyValue = null;
                strKeyValue = enc.GetBytes(CURRENT_KEY);

                lsExpresion = lsExpresion.Replace("\r\n", string.Empty);
                string lsResponse = Cryptology.Decrypt(lsExpresion, CURRENT_KEY);
                lsExpresion = lsResponse;

            }
            return (lsExpresion);
        }
    }
}
